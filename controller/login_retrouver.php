<?php

# Require PDO request library
require_once("../shurti89/Db.class.php");

# The instance
$db = new DB_CLASS();

# Function Modele
include_once '../modeles/DataForget.php';

$response = array();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!empty($_POST['v_tel']) && !empty($_POST['v_date_naiss']) && !empty($_POST['v_pwd']) && !empty($_POST['v_email'])) {

        $numero_tel = $_POST['v_tel'];

        $password = $_POST['v_pwd'];
        $naissance = $_POST['v_date_naiss'];
        $emailPat = $_POST['v_email'];

        $ligCheck = checkUser($numero_tel, $password, $naissance);

        if ($ligCheck != false) {

            //$ligverificationne = checkUser($numero_tel, $password, $naissance);

            $idPat = $ligCheck->IDPAT;

            $loginpat = $ligCheck->LOGIN;

            if (!empty($loginpat) && !empty($idPat)) {

                $request = mise_a_jour_code_secret($password, $idPat);

                if ($request == true) {

                    send_reset_email($loginpat, $password, $emailPat);

                    $response['status'] = 1;

                    $response['message'] = "Votre login est : " . $loginpat . "";

                } else {

                    $response['status'] = 2;

                    $response['message'] = "Impossible d'effectué la reinitialisation de votre Mot de Passe. Erreur P3045";
                }
            } else {

                $response['status'] = 0;

                $response['message'] = "Impossible de recuperer les informations pour la mise à jour des données.";

            }
        } else {

            $response['status'] = 0;

            $response['message'] = "Abonné non identifié";
        }
    }
}

echo json_encode($response, JSON_UNESCAPED_UNICODE);

?>
