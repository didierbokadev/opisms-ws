<?php /** @noinspection ALL */

# Require PDO request library
require_once("../shurti89/Db.class.php");

# The instance
$db = new DB_CLASS();

# Function Modele
include_once '../modeles/UserSetting.php';

$response = array();


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    if (!empty($_POST['user_login']) && !empty($_POST['user_password'])) {
        $UserLogin = login(trim($_POST['user_login']), trim($_POST['user_password']));

        if ($UserLogin) {
            $get_profil = getProfil($UserLogin->IDPAT);
            $patient = array();
            $patient["id_pat"] = $get_profil->IDPAT;
            $patient["nom"] = $get_profil->NOMPAT;
            $patient["prenom"] = $get_profil->PRENOMPAT;

            if ($get_profil->DATEPAT == '0000-00-00') {
                $datNaiss = 'NON DEFINI';
            } else if ($get_profil->DATEPAT != '0000-00-00') {
                $nele = explode('-', $get_profil->DATEPAT);
                $datNaiss = $nele[2] . '-' . $nele[1] . '-' . $nele[0];
            }

            $patient["datenais"] = $datNaiss;
            $patient["sexe"] = $get_profil->SEXEPAT;
            $patient["contact"] = $get_profil->NUMEROPAT;

            if ($get_profil->EMAILPAT == '') {
                $email = 'NON DEFINI';
            } else if ($get_profil->EMAILPAT != '') {
                $email = $get_profil->EMAILPAT;
            }

            $patient["email"] = $email;
            $dAb = explode('-', $get_profil->DATE_ABONN);
            $data = explode('-', $get_profil->DATESAISIE);
            $patient["abonnement"] = $dAb[2] . '-' . $dAb[1] . '-' . $dAb[0];
            $patient["expiration"] = ($dAb[2] - 1) . '-' . $dAb[1] . '-' . ($dAb[0] + $get_profil->VALIDITE);
            $patient["recu"] = $get_profil->NUMRECU;
            $patient["activite"] = $get_profil->ACTIVITE;

            if ($get_profil->PROVENANCE == '') {
                $provenance = 'NON DEFINI';
            } else if ($get_profil->PROVENANCE != '') {
                $provenance = $get_profil->PROVENANCE;
            }

            $patient["provenance"] = $provenance;
            $patient["date_saisie"] = $data[2] . '-' . $data[1] . '-' . $data[0];
            $patient["username"] = $get_profil->LOGIN;
            $patient["password"] = $_POST['user_password'];

            $response['TProfil'] = array();
            $response['status'] = 1;

            $response['message'] = "Connexion réussi";
            array_push($response['TProfil'], $patient);
        } else if ($UserLogin == false) {
            $response['status'] = 0;
            $response['message'] = "Abonné non identifié";
        }
    }
}

echo json_encode($response, JSON_UNESCAPED_UNICODE);

?>
