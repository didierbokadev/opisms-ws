<?php

	# Require PDO request library
	require_once("../shurti89/Db.class.php"); 

	# The instance
	$db = new DB_CLASS(); 
	
	# Function Modele
	include_once '../modeles/EditProfil.php';

	$response = array();
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		if(!empty($_POST['vName']) && !empty($_POST['vLastName']) && !empty($_POST['vNeLe']) && !empty($_POST['vSexe']) && !empty($_POST['vProfess']) && !empty($_POST['vIdPAt']))
		{
			
			$nom_patient = strtoupper($_POST['vName']);
			
			$prenom_patient = strtoupper($_POST['vLastName']);
			
			$sex_patient = $_POST['vSexe'];
			
			$profession_patient = $_POST['vProfess'];
			
			$idPat = $_POST['vIdPAt'];
			
			$ligCheck = getProfil($idPat);
			
			if($ligCheck != false) {
				
				//Explode de la date
				list($jour2, $mois2, $annee2) = explode("-", $_POST['vNeLe']);
        
				$ne_le_patient = ($annee2."-".$mois2."-".$jour2);
				
				$edit = mise_a_jour_profil($nom_patient, $prenom_patient, $ne_le_patient, $sex_patient, $profession_patient, $idPat);
								
				if($edit == true){
					
					if(empty($ligCheck->EMAILPAT))
					{
						
						$response['status'] = 1;
				
						$response['message'] = "La mise à jour de votre profil effectué avec succès";
					
					}
					else if(!empty($ligCheck->EMAILPAT)){
						
						$titre = "Mise à Jour profil > OPISMS VACCIN";
						
						$txt = "La mise à jour de vos informations de base a été effectuées avec succès.";
						
						send_email($ligCheck->EMAILPAT, $titre, $txt);
						
						$response['status'] = 1; 
				
						$response['message'] = "La mise à jour de votre profil effectué avec succès. Un E-mail vous a été adressé à ce propos.";
					}
				}
				else {
					$response['status'] = 2;
				
					$response['message'] = "Impossible d'effectué la mise à jour de votre profil. Erreur P3048";
				}				
			}
			else {
				
				$response['status'] = 0;
				
				$response['message'] = "Abonné non identifié";
			}
		}
    }

    echo json_encode($response, JSON_UNESCAPED_UNICODE);
