<?php

	# Require PDO request library
	require_once("../shurti89/Db.class.php"); 

	# The instance
	$db = new DB_CLASS(); 
	
	# Function Modele
	include_once '../modeles/DataForget.php';

	$response = array();


    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		if(!empty($_POST['username']) && !empty($_POST['date_naiss'])){
			
			if(verification($_POST['username'], $_POST['date_naiss']) != false) {
				
				$ligverificationne = getIdPatProfil($_POST['username'], $_POST['date_naiss']);
				
				$idPat = $ligverificationne->IDPAT;				
				
				$passwordRestet = GeraHash(5);
				
				if(!empty($passwordRestet)){
					
					$request = mise_a_jour_code_secret($passwordRestet, $idPat);
					
					if($request == true){
						
						$response['status'] = 1;
						
						$response['message'] = "Votre nouveau a été réinitialiser avec succès. Votre nouveau mot de passe est : ".$passwordRestet."";

					}
					else{
						
						$response['status'] = 2;
				
						$response['message'] = "Impossible d'effectué la reinitialisation de votre Mot de Passe. Erreur P3045";
					}
				}								
			}
			else {
				
				$response['status'] = 0;
				
				$response['message'] = "Abonné non identifié";
			}
		}
    }

    echo json_encode($response, JSON_UNESCAPED_UNICODE);

?>
