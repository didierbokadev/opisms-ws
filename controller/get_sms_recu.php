<?php

	# Require PDO request library
	require_once("../shurti89/Db.class.php"); 

	# The instance
	$db = new DB_CLASS(); 
	
	# Function Modele
	include_once '../modeles/GettingData.php';

	$response = array();
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		if(!empty($_POST['idPat'])){
			
			$ligCheck = getProfil($_POST['idPat']);
			
			if($ligCheck != false) {

				$get_sms = getSms($_POST['idPat']);
				
				if($get_sms != false){
					
					$response['TSms'] = array();

					foreach ($get_sms as $sql){
						
						$sms = array();
						
						$sms["idMsg"] = $sql->ID_MSG;
						
						$sms["message"] = $sql->MESSAGE;
						
						$dat = explode ('-', $sql->DateMSG);
						
						$sms["dateMsg"] = $dat[2].'-'.$dat[1].'-'.$dat[0];
						
						$sms["heurMsg"] = $sql->HEUREMSG;
						
						$sms["dat_envoi"] = $sql->DATE_ENVOI;
						
						$sms["idClient"] = $_POST['idPat'];
						
						array_push($response["TSms"], $sms);
					}
					
					$response['status'] = 1;
				
					$response['message'] = "La recupération des sms reçus a été effectué avec succès";
				}
				else if($get_sms == false){
					
					$response['status'] = 1;
				
					$response['message'] = "Vous n'avez aucune liste d'sms liée à votre compte OPISMS VACCIN.";
				}
			}
			else {
				
				$response['status'] = 0;
				
				$response['message'] = "Abonné non identifié, Veuillez contacter le service commercial";
			}
		}
		else {
				
			$response['status'] = 0;
			
			$response['message'] = "Impossible de traiter la demande";
		}		
	}
	
	echo json_encode($response, JSON_UNESCAPED_UNICODE);
