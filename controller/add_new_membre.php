<?php

	# Require PDO request library
	require_once("../shurti89/Db.class.php"); 

	# The instance
	$db = new DB_CLASS(); 
	
	# Function Modele
	include_once '../modeles/GettingData.php';	
	include_once '../modeles/AddMembre.php';

	$response = array();
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		if (!empty($_POST['numrecu']) && !empty($_POST['numphone']) && !empty($_POST['idParrain'])) {
			
			$ligCheck = verification (strtoupper($_POST['numrecu']), $_POST['numphone']);
			
			if($ligCheck != false) { 
				
				$update = ajout_membre ($_POST['idParrain'], $ligCheck->IDPAT);
				
				if($update){
					
					//Recupération des sms reçu par le Nouveau membre de Famille
					$get_sms = getSms($ligCheck->IDPAT);
					
					$response["TSmsNewMembre"] = array();
					
					foreach ($get_sms as $sql){
						
						$smsMF = array();
								
						$smsMF["idMsg"] = $sql->ID_MSG;
						
						$smsMF["message"] = $sql->MESSAGE;
						
						$dat = explode ('-', $sql->DateMSG);
						
						$smsMF["dateMsg"] = $dat[2].'-'.$dat[1].'-'.$dat[0];
						
						$smsMF["heurMsg"] = $sql->HEUREMSG;
						
						$smsMF["dat_envoi"] = $sql->DATE_ENVOI;
						
						$smsMF["idClient"] = $_POST['idParrain'];
						
						$smsMF["idFilleulSMF"] = $ligCheck->IDPAT;
						
						array_push($response["TSmsNewMembre"], $smsMF);
					}
					
					
					//Recupération du calendrier vaccinal du Nouveau membre de Famille
					
					$get_calandrier = getCalendrier($ligCheck->IDPAT);
					
					$response["TCalendrierNewMembre"] = array();
					
					foreach ($get_calandrier as $sql){
						
						$calendrierMF = array(); 
								
						$calendrierMF["idCal"] = $sql->IDCAL;
						
						$calendrierMF["nomVaccin"] = $sql->NOMVAC;
						
						$dat = explode ('-', $sql->DATERAPEL);
						
						$calendrierMF["dateRappel"] = $dat[2].'-'.$dat[1].'-'.$dat[0];
						
						$datPresence = explode ('-', $sql->PRESENCE);
						
						$calendrierMF["presence"] = $datPresence[2].'-'.$datPresence[1].'-'.$datPresence[0];
						
						$calendrierMF["lotVaccin"] = $sql->LOVAC;
						
						$calendrierMF["nomCentre"] = $sql->NOMCENTR;
						
						$calendrierMF["idAbonne"] = $_POST['idParrain'];
						
						$calendrierMF["idFilleulCalVaccMF"] = $ligCheck->IDPAT;
						
						array_push($response["TCalendrierNewMembre"], $calendrierMF);
					}
					
					
					$newMemebre = array();
					
					$newMemebre["idMembre"] = $ligCheck->IDPAT;
				
					$newMemebre["nom_membre"] = $ligCheck->NOMPAT;
					
					$newMemebre["prenom_membre"] = $ligCheck->PRENOMPAT;
					
					$dat = explode ('-', $ligCheck->DATEPAT);
						
					$newMemebre["date_naiss_membre"] = $dat[2].'-'.$dat[1].'-'.$dat[0];
					
					$newMemebre["sexe_membre"] = $ligCheck->SEXEPAT;	
					
					$newMemebre["idParrain"] = $_POST['idParrain'];				
					
					$response["TNewMembre"] = array();
					
					array_push($response["TNewMembre"], $newMemebre);
					
					
					if(empty($ligCheck->EMAILPAT)){
							
						$response['status'] = 1;
				
						$response['message'] = "Ajout du nouveau membre effectué avec succès.";
					
					}
					else if(!empty($ligCheck->EMAILPAT)){
						
						$titre = "Ajout d'un nouveau Membre > OPISMS VACCIN";
						
						$txt = "L'ajout du nouveau membre du nom de ".$ligCheck->NOMPAT." ".$ligCheck->PRENOMPAT." à été éffectué avec succès.";
						
						send_email($ligCheck->EMAILPAT, $titre, $txt);
						
						$response['status'] = 1; 
				
						$response['message'] = "Ajout effectué avec succès. Un E-mail vous a été adressé à ce propos.";
					}
				}
				else{
					
					$response['status'] = 2;
				
					$response['message'] = "Impossible d'effectué l'ajout du nouveau membre, Veuillez réessayer. Erreur P3049";
				}
			}
			else {
				
				$response['status'] = 0;
				
				$response['message'] = "Les informations soumises sont invalides, Impossible de traiter votre requête.";
			}			
		}		 
	}
	
	echo json_encode($response, JSON_UNESCAPED_UNICODE);
