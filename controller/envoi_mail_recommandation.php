<?php

	# Require PDO request library
	require_once("../shurti89/Db.class.php"); 

	# The instance
	$db = new DB_CLASS(); 
	
	# Function Modele
	include_once '../modeles/EnvoiRecommandation.php';

	$response = array();
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		if (!empty($_POST['Email']) && !empty($_POST['Objet']) && !empty($_POST['Motif']) && !empty($_POST['Message'])) {
			
			envoi_de_la_preoccupation('info@opisms.org', $_POST['Objet'], $_POST['Message'], strtoupper($_POST['Motif']), strtoupper($_POST['NomAbonne']), strtoupper($_POST['PrenomsAbonne']), $_POST['Email'], $_POST['Contact']);
			
			$titre = "Envoi d'une préoccupation > OPISMS VACCIN";
						
			$txt = "Votre préoocupation à été prise en conte avec succès.";
			
			send_email($_POST['Email'], $titre, $txt);
				
			$response['status'] = 1;
			
			$response['message'] = "Votre préoocupation à été prise en conte avec succès. Nous vous reviendrons.";
		
		}	
		else {
				
			$response['status'] = 2;
				
			$response['message'] = "Impossible de traiter votre demande, Veuillez réessayer ultérieurement.";
		}	 
	}
	
	echo json_encode($response, JSON_UNESCAPED_UNICODE);
