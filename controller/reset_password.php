<?php

	# Require PDO request library
	require_once("../shurti89/Db.class.php"); 

	# The instance
	$db = new DB_CLASS(); 
	
	# Function Modele
	include_once '../modeles/EditProfil.php';

	$response = array();
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		if(!empty($_POST['idPat']))
		{
			$idPat = $_POST['idPat'];
			
			$newPassword = "12345";
			
			$ligCheck = getProfil($idPat);
			
			if($ligCheck != false) {
				
				$request = mise_a_jour_code_secret($newPassword, $idPat);
					
				if($request == true){
					
					if(empty($ligCheck->EMAILPAT))
					{
						
						$response['status'] = 1;
				
						$response['message'] = "La Reinitialisation de votre mot de passe effectué avec succès";
					
					}
					else if(!empty($ligCheck->EMAILPAT)){
						
						$titre = "Reinitialisation de votre Mot de Passe > OPISMS VACCIN";
						
						$txt = "La reinitialisation de votre mot de passe a été effectué avec succès.";
						
						send_email($ligCheck->EMAILPAT, $titre, $txt);
						
						$response['status'] = 1; 
				
						$response['message'] = "La Reinitialisation de votre Mot de passe effectué avec succès. Un E-mail vous a été adressé à ce propos.";
					}
				}
				else{
					
					$response['status'] = 2;
			
					$response['message'] = "Impossible d'effectué la reinitialisation de votre Mot de Passe. Erreur P3055";
				}				
			}
			else {
				
				$response['status'] = 0;
				
				$response['message'] = "Abonné non identifié";
			}
		}
	}
	
	echo json_encode($response, JSON_UNESCAPED_UNICODE);
