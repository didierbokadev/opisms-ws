<?php
	# Require PDO request library
	require_once("../shurti89/Db.class.php"); 

	# The instance
	$db = new DB_CLASS(); 
	
	# Function Modele
	include_once '../modeles/ReabonnementUser.php';

	$response = array();
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		if(!empty($_POST['idPat']) && !empty($_POST['token']) && !empty($_POST['nbre'])){
			
			$get_profil = user_data (intval($_POST['idPat']));
			
			if($get_profil){
				$dateAbonn = date('Y-m-d');
				$nbre_annee  = intval($_POST['nbre']);
				
				$tarif = $nbre_annee * 1000;
				$dateExp = date('Y-m-d', strtotime('+'.$nbre_annee.' years', strtotime($dateAbonn)));

				$compte = verifie_insertion_abonnement($_POST['token']);				
				
				if ($compte) {
					$response['status'] = 2;
					$response['message'] = "Désoler votre réabonnement à déjà été prise en compte par notre système.";
				} else {
				
					//Mise en place du traitement
					$reabonnement = insert_new_t_abonnement ($get_profil->IDPAT, $dateAbonn, 0, $_POST['token'], $get_profil->IDGERANTP, $nbre_annee, $tarif, $get_profil->IDCENTRE, 0, 0, 0, 2);
					
					if ($reabonnement) {
						$actiavtion = actiavtion_du_compte (1, $get_profil->IDPAT);
						if ($actiavtion) {
							
							//Envoi de message							
							$expiration = date('d/m/Y', strtotime($dateExp));
							
							$dato = explode ('-', $dateExp);							
							
							//$messagetex = $get_profil->PRENOMPAT.', VOTRE REABONNEMENT AU SERVICE DU CARNET DE VACCINATION ELECTRONIQUE A BIEN ETE EFFECTUE. VALABLE JUSQU\'AU '. $dat[2].'/'.$dat[1].'/'.$dat[0] .' . OPISMS VACCIN VOUS REMERCIE. 22521243489';
							$messagetex = $get_profil->PRENOMPAT.', VOTRE ABONNEMENT AU CARNET ELECTRONIQUE DE VACCINATION EST RECONDUIT POUR ' . $nbre_annee . ' AN(S). ECHEANCE LE ' . $dato[2].'-'.$dato[1].'-'.$dato[0] . '. OPISMS VACCIN VOUS REMERCIE. 22521243489';
														
							$insertMessage = insert_new_t_msg_envoyes($get_profil->IDPAT, $messagetex, date('Y-m-d'), date('H:i:s'), $get_profil->NUMEROPAT, "NT", "RBN", 1, "VAC", date('Y-m-d H:i:s'), 0, "", 0, reseau($get_profil->NUMEROPAT), "500", "CINETPAY", "CINETPAY");
							
							if ($insertMessage) {
								
								$insert = last_insert_t_msg_envoye ($get_profil->IDPAT);
								
								if($insert){
									
									if(empty($get_profil->EMAILPAT)){
										
										$response['TSms'] = array();
									
										$sms = array();
										
										$sms["idMsg"] = $insert->ID_MSG;
							
										$sms["message"] = $messagetex;
										
										$dat = explode ('-', $insert->DateMSG);
										
										$sms["dateMsg"] = $dat[2].'-'.$dat[1].'-'.$dat[0];
										
										$sms["heurMsg"] = $insert->HEUREMSG;
										
										$sms["dat_envoi"] = $insert->DATE_ENVOI;
										
										$sms["idClient"] = $get_profil->IDPAT;
										
										array_push($response["TSms"], $sms);
							
										
										$infos2 = json_encode($reabonnement, true)."//";

										$monfichier2 = fopen('notificateur.txt', 'r+');

										fseek($monfichier2, 0); // On remet le curseur au début du fichier

										fputs($monfichier2, $infos2); // On écrit le nouveau nombre de pages vues

										fclose($monfichier2);
										
										
										$response['status'] = 1;
								
										$response['message'] = "Votre reabonnement au service du carnet de vaccination electronique a bien ete effectue.";
										
									}
									else if(!empty($get_profil->EMAILPAT)){
										
										$response['TSms'] = array();
									
										$sms = array();
										
										$sms["idMsg"] = $insert->ID_MSG;
							
										$sms["message"] = $messagetex;
										
										$dat = explode ('-', $insert->DateMSG);
										
										$sms["dateMsg"] = $dat[2].'-'.$dat[1].'-'.$dat[0];
										
										$sms["heurMsg"] = $insert->HEUREMSG;
										
										$sms["dat_envoi"] = $insert->DATE_ENVOI;
										
										$sms["idClient"] = $get_profil->IDPAT;
										
										array_push($response["TSms"], $sms);
										
										
										$titre = "Mise à Jour profil > OPISMS VACCIN";
										
										mail_de_reabonnement($get_profil->EMAILPAT, $titre, $messagetex);
										
										
										
										$infos2 =json_encode($reabonnement, true)."//";

										$monfichier2 = fopen('notificateur.txt', 'r+');

										fseek($monfichier2, 0); // On remet le curseur au début du fichier

										fputs($monfichier2, $infos2); // On écrit le nouveau nombre de pages vues

										fclose($monfichier2);
																				
										
										$response['status'] = 1; 
								
										$response['message'] = "Votre reabonnement au service du carnet de vaccination electronique a bien ete effectue. Un E-mail vous a été adressé à ce propos.";
									}
								}
								else{
									
									$response['status'] = 2;
					
									$response['message'] = "Échec de l'exécution de la requête.";
								}
								
							} else {
								
								$response['status'] = 2;
					
								$response['message'] = "Impossible de traiter pour la mise à disposition du message de reabonnement. Erreur P3047";
							}
						}
						else{		
											
							$response['status'] = 2;
					
							$response['message'] = "Impossible de traiter votre requête d'activation du compte. Erreur P3049";
						}						
					}
					else{
						
						$response['status'] = 2;
					
						$response['message'] = "Impossible de traiter votre requête liée à la création d'une nouvelle ligne d'abonnement. Erreur P3048";
					}
				}							
			}
			else if($get_profil == false){
									
				$response['status'] = 0;
				
				$response['message'] = "Abonné non identifié";
			}			
		}		
	}
	
	
	echo json_encode($response, JSON_UNESCAPED_UNICODE);
