<?php

	# Require PDO request library
	require_once("../shurti89/Db.class.php"); 

	# The instance
	$db = new DB_CLASS(); 
	
	# Function Modele
	include_once '../modeles/GettingData.php';

	$response = array();
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		if(!empty($_POST['idPat'])){
			
			$ligCheck = getProfil($_POST['idPat']);
			
			if($ligCheck != false) {
				
				$get_calandrier = getCalendrier($_POST['idPat']);
				
				if($get_calandrier != false){
				
					$response['TCalendrier'] = array();
					
					foreach ($get_calandrier as $sql){
						
						$calendrier = array(); 
						
						$calendrier["idCal"] = $sql->IDCAL;
						
						$calendrier["nomVaccin"] = $sql->NOMVAC;
						
						$dat = explode ('-', $sql->DATERAPEL);
						
						$calendrier["dateRappel"] = $dat[2].'-'.$dat[1].'-'.$dat[0];
						
						$datPresence = explode ('-', $sql->PRESENCE);
						
						$calendrier["presence"] = $datPresence[2].'-'.$datPresence[1].'-'.$datPresence[0];
						
						$calendrier["lotVaccin"] = $sql->LOVAC;
						
						$calendrier["nomCentre"] = $sql->NOMCENTR;
						
						$calendrier["idAbonne"] = $sql->IDPAT;
						
						array_push($response["TCalendrier"], $calendrier);
					}
					
					$response['status'] = 1;
				
					$response['message'] = "La recupération de votre carnet de vaccination électronique a été effectué avec succès.";
				}
				else if($get_calandrier == false){
					
					$response['status'] = 1;
				
					$response['message'] = "Le calendrier vaccinnal lié à votre compte OPISMS VACCIN est vide actuellement.";
				} 
			}
			else {
				
				$response['status'] = 0;
				
				$response['message'] = "Abonné non identifié, Veuillez contacter le service commercial";
			}			
		}
		else {
				
			$response['status'] = 0;
			
			$response['message'] = "Impossible de traiter la demande";
		}		
	}
	
	echo json_encode($response, JSON_UNESCAPED_UNICODE);
