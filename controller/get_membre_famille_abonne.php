<?php

	# Require PDO request library
	require_once("../shurti89/Db.class.php"); 

	# The instance
	$db = new DB_CLASS(); 
	
	# Function Modele
	include_once '../modeles/GettingData.php';

	$response = array();
	
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		
		if(!empty($_POST['idPat'])){
			
			$ligCheck = getProfil($_POST['idPat']);
			
			if($ligCheck != false) { 
				
				$get_membre = getMembreFamille($_POST['idPat']);
				
				if($get_membre != false){
					
					$response['TMembreFamille'] = array();
					
					$response['TSmsMF'] = array();
					
					$response['TCalVaccMF'] = array();
					
					foreach ($get_membre as $sql){
						
						$membre = array(); 
						
						$membre["idMembre"] = $sql->IDPAT;
						
						$membre["nom_membre"] = $sql->NOMPAT;
						
						$membre["prenom_membre"] = $sql->PRENOMPAT;
						
						$dat = explode ('-', $sql->DATEPAT);
						
						$membre["date_naiss_membre"] = $dat[2].'-'.$dat[1].'-'.$dat[0];
						
						$membre["sexe_membre"] = $sql->SEXEPAT;
						
						$membre["idParrain"] = $_POST['idPat'];
						
						array_push($response["TMembreFamille"], $membre);						
						
					}

					foreach ($response["TMembreFamille"] as $value){

						$data = explode("/", $value["idMembre"].'/');						
						
						foreach($data as $cle => $element)
						{
							//Sms recupar les Membres de la famille
							$get_sms = getSms($element);

							foreach ($get_sms as $sql){
								
								$smsMF = array();
								
								$smsMF["idMsg"] = $sql->ID_MSG;
								
								$smsMF["message"] = $sql->MESSAGE;
								
								$dat = explode ('-', $sql->DateMSG);
								
								$smsMF["dateMsg"] = $dat[2].'-'.$dat[1].'-'.$dat[0];
								
								$smsMF["heurMsg"] = $sql->HEUREMSG;
								
								$smsMF["dat_envoi"] = $sql->DATE_ENVOI;
								
								$smsMF["idClient"] = $_POST['idPat'];
								
								$smsMF["idFilleulSMF"] = $element;
								
								array_push($response["TSmsMF"], $smsMF);
							}
							
							
							//Calendrier vaccinal des Membres de la famille
							$get_calandrier = getCalendrier($element);
							
							foreach ($get_calandrier as $sql){
								
								$calendrierMF = array(); 
								
								$calendrierMF["idCal"] = $sql->IDCAL;
								
								$calendrierMF["nomVaccin"] = $sql->NOMVAC;
								
								$dat = explode ('-', $sql->DATERAPEL);
								
								$calendrierMF["dateRappel"] = $dat[2].'-'.$dat[1].'-'.$dat[0];
								
								$datPresence = explode ('-', $sql->PRESENCE);
								
								$calendrierMF["presence"] = $datPresence[2].'-'.$datPresence[1].'-'.$datPresence[0];
								
								$calendrierMF["lotVaccin"] = $sql->LOVAC;
								
								$calendrierMF["nomCentre"] = $sql->NOMCENTR;
								
								$calendrierMF["idAbonne"] = $_POST['idPat'];
								
								$calendrierMF["idFilleulCalVaccMF"] = $element;
								
								array_push($response["TCalVaccMF"], $calendrierMF);
							}
						}
					}
					
					if(empty($ligCheck->EMAILPAT)){
							
						$response['status'] = 1;
				
						$response['message'] = "La configuration de votre mobile a été effectué avec succès.";
					
					}
					else if(!empty($ligCheck->EMAILPAT)){
						
						$titre = "Configuration de votre Mobile > OPISMS VACCIN";
						
						$txt = "La configuration de votre mobile a été effectué avec succès.";
						
						send_email($ligCheck->EMAILPAT, $titre, $txt);
						
						$response['status'] = 1; 
				
						$response['message'] = "La configuration de votre mobile pour le module Famille a été effectué avec succès. Un E-mail vous a été adressé à ce propos.";
					}
					
				}
				else if($get_membre == false){
					
					$response['status'] = 1;
				
					$response['message'] = "Vous n'avez associer Aucun membre de votre famille à votre compte OPISMS VACCIN.";
				} 				
			}
			else {
				
				$response['status'] = 0;
				
				$response['message'] = "Abonné non identifié, Veuillez contacter le service commercial";
			}			
		}
		else {
				
			$response['status'] = 0;
			
			$response['message'] = "Impossible de traiter la demande";
		}		
	}
	
	echo json_encode($response, JSON_UNESCAPED_UNICODE);
