<?php

    /**
     * Created by PhpStorm.
     * User: ADMIN
     * Date: 12/09/2018
     * Time: 10:59
     */

    use Slim\Http\Request;
    use Slim\Http\Response;


    // region IMPORTS
    require 'modeles/EnvoiRecommandation.php';
    require 'database.php';
    require 'functions.php';
    // endregion


// region API PRESENTATION
    $app->get('/', function (Request $request, Response $response) {
        return $response->withJson("Welcome to OpiSms API");
    });

    $app->get('/tables', function (Request $request, Response $response) {
        return $response->withJson("");
    });

    $app->get('/api', function (Request $request, Response $response) {
        return $response->withJson("Welcome to OpiSms API");
    });

    $app->get('/api/', function (Request $request, Response $response) {
        return $response->withJson("Welcome to OpiSms API");
    });

    $app->get('/api/v1', function (Request $request, Response $response) {
        return $response->withJson("Welcome to OpiSms API");
    });

    $app->get('/api/v1/', function (Request $request, Response $response) {
        return $response->withJson("Welcome to OpiSms API");
    });
// endregion


// region PRODUCTION
    $app->group('/api/v1', function () {

        $this->get('/tables', function (Request $request, Response $response) {
            $databaseInstance = $this->dev;
            print_r($databaseInstance);
            //return $response->withJson($databaseInstance);
        });

        // User login
        $this->post('/user/login', function (Request $request, Response $response) {
            $reponse = [];
            $datas = (object) $request->getParsedBody();

            if ($datas->d == 'DEV') $databaseInstance = $this->dev;
            if ($datas->d == 'PROD') $databaseInstance = $this->prod;

            // Get datas user send
            $login = $datas->login;
            $password = $datas->password;

            if (empty($login)) {
                $reponse['code'] = 2;
                $reponse['msg'] = "Utilisateur inconnu ou Login/mot de passe incorrect !";
                return $response->withJson($reponse);
            }

            $user  = $databaseInstance->query("SELECT * 
                                               FROM info_patient InfPat 
                                                   INNER JOIN patient Pat ON Pat.IDPAT = InfPat.IDPAT 
                                               WHERE (InfPat.EMAILPAT = '" . $login . "' OR Pat.LOGIN = '" . $login . "') AND Pat.PASSWORD = '" . md5($password) . "'")->fetch();


            // Check user status
            if ($user->IDPAT > 0) {
                $reponse['code'] = 0;
                $reponse['msg'] = "User found";

                if ($user->statut == 1) {
                    // Find user infos
                    $sqlUserInfos = $databaseInstance->query("SELECT A.IDPAT, A.DATE_ABONN, A.NUMRECU, A.VALIDITE, A.DATE_EXPIRATION, A.IDFORMULE, A.ID_ABONN
                                                              FROM t_abonnement A
                                                              WHERE A.IDPAT = $user->IDPAT
                                                              ORDER BY A.ID_ABONN DESC LIMIT 1")->fetch();

                    $user->DATE_ABONN = $sqlUserInfos->DATE_ABONN;
                    $user->NUMRECU = $sqlUserInfos->NUMRECU;
                    $user->VALIDITE = $sqlUserInfos->VALIDITE;
                    $user->DATE_EXPIRATION = $sqlUserInfos->DATE_EXPIRATION;
                    $user->IDFORMULE = $sqlUserInfos->IDFORMULE;
                    $reponse['data'] = $user;
                } else {
                    $reponse['code'] = 1;
                    $reponse['msg'] = "Votre compte est inactif";
                }
            } else {
                $reponse['code'] = 2;
                $reponse['msg'] = "Utilisateur inconnu ou Login/mot de passe incorrect !";
            }

            return $response->withJson($reponse);
        });

        $this->post('/user/calendriers', function (Request $request, Response $response) {
            $datas = (object) $request->getParsedBody();

            $patientId = $datas->patientId;

            // Var return
            $reponse = [];

            // Database instance

            if ($datas->d == 'DEV') $databaseInstance = $this->dev;
            if ($datas->d == 'PROD') $databaseInstance = $this->prod;

            $sqlCalendriers = "SELECT IDCAL, NOMVAC, DATERAPEL, PRESENCE, LOVAC, NOMCENTR, IDPAT 
                               FROM calendrier, centre, vaccin 
                               WHERE  centre.IDCENTR = calendrier.IDCENTR 
                               AND vaccin.IDVAC = calendrier.IDVAC
                               AND calendrier.IDPAT = " . $patientId;

            $calendriers = $databaseInstance->query($sqlCalendriers)->fetchAll();

            if (count($calendriers) > 0) {
                $reponse['code'] = 0;
                $reponse['msg'] = "Liste des vaccins effectues";
                $reponse['data'] = [];

                foreach ($calendriers as $index => $calendrier) {
                    $reponse['data'][] = $calendrier;
                }
            } else {
                $reponse['code'] = 1;
                $reponse['msg'] = 'Pas de vaccin';
            }

            return $response->withJson($reponse);
        });

        $this->post('/user/sms', function (Request $request, Response $response) {
            $datas = (object)$request->getParsedBody();

            if ($datas->d == 'DEV') $databaseInstance = $this->dev;
            if ($datas->d == 'PROD') $databaseInstance = $this->prod;

            $patientId = $datas->patientId;

            $reponse = [];

            $sqlMessages = "SELECT M.ID_MSG, M.MESSAGE, M.DateMSG, M.HEUREMSG, M.DATE_ENVOI 
                            FROM t_msg_envoyes M, patient P 
                            WHERE (M.IDCLT = " . $patientId . " AND P.IDPAT = M.IDCLT)
                            ORDER BY M.DateMSG DESC";

            $messages = $databaseInstance->query($sqlMessages)->fetchAll();

            if (count($messages) > 0) {
                $reponse['code'] = 0;
                $reponse['msg'] = 'Liste des messages';
                $reponse['data'] = [];

                foreach ($messages as $index => $message) {
                    array_push($reponse['data'], $message);
                }
            } else {
                $reponse['code'] = 1;
                $reponse['msg'] = 'Aucun message enregrtre.';
            }

            return $response->withJson($reponse);
        });

        $this->post('/user/families', function (Request $request, Response $response) {
            $datas = (object) $request->getParsedBody();

            if ($datas->d == 'DEV') $databaseInstance = $this->dev;
            if ($datas->d == 'PROD') $databaseInstance = $this->prod;

            $patientId = $datas->patientId;

            $reponse = [];

            $sqlFamilies = "SELECT IDPAT, NOMPAT, PRENOMPAT, SEXEPAT, DATEPAT 
                            FROM info_patient 
                            WHERE info_patient.PARENT_ID = " . $patientId . " AND IDPAT != " . $patientId;
            $families = $databaseInstance->query($sqlFamilies)->fetchAll();

            if (count($families) > 0) {
                $reponse['code'] = 0;
                $reponse['msg'] = 'Liste des membres de famille';
                $reponse['data'] = [];

                foreach ($families as $index => $family) {
                    $sqlResponse = $databaseInstance->query("SELECT DATE_ABONN, DATE_EXPIRATION FROM t_abonnement WHERE IDPAT = " .$family->IDPAT. " ORDER BY ID_ABONN DESC LIMIT 1")->fetch();
                    $family->DATEABONNEMENT = DateTime::createFromFormat('Y-m-d', $sqlResponse->DATE_ABONN)->format('d-m-Y');
                    $family->DATEEXPIRATION = DateTime::createFromFormat('Y-m-d', $sqlResponse->DATE_EXPIRATION)->format('d-m-Y');

                    $reponse['data'][] = $family;
                }
            } else {
                $reponse['code'] = 1;
                $reponse['msg'] = 'Pas de membre';
            }

            return $response->withJson($reponse);
        });

        $this->post('/user/formule/update', function (Request $request, Response $response) {


            $datas = (object) $request->getParsedBody();


            if ($datas->d == 'DEV') $databaseInstance = $this->dev;
            if ($datas->d == 'PROD') $databaseInstance = $this->prod;

            $patientInstance = json_decode($datas->patient);
            $formatDate = "Y-m-d";

            $flagSmsAbon    =   1;
            $originAbnt     =   3;
            $flagMsgVocal   =   2;
            $infoSanteId    =   68;
            $flagAbmt       =   2;
            $formule        =   10;
            $gerandId       =   1;
            $validite       =   $patientInstance->VALIDITE;
            $alertDesab     =   -3;
            $montant        =   floatval(5500 * $validite);
            $recuId         =   0;
            $recuNumber     =   rand(111111, 999999);

            if (is_null($patientInstance->NUMEROPAT2)) {
                $patientInstance->NUMEROPAT2 = "";
            }

            $dateInstance = new DateTime('Africa/Abidjan');
            $dateNow = $dateInstance->format('Y-m-d');
            // Find last subcription
            $sqlLastSubscription = "SELECT * FROM t_abonnement WHERE IDPAT = " . $patientInstance->IDPAT . " ORDER BY ID_ABONN DESC LIMIT 1";
            $stmtLastSubscrition = $databaseInstance->query($sqlLastSubscription)->fetch();

            $subscriptionDateExp = DateTime::createFromFormat($formatDate, $stmtLastSubscrition->DATE_EXPIRATION);
            $dateNow = DateTime::createFromFormat($formatDate, $dateNow);

            if ($dateNow > $subscriptionDateExp) {
                $subscriptionNewdateExp = $dateNow->modify('+ ' . $validite .'years')->format($formatDate);
            } else {
                $subscriptionNewdateExp = $subscriptionDateExp->modify('+ ' . $validite .'years')->format($formatDate);
            }

            //$sqlFormuleUpdate = "UPDATE t_abonnement SET DATE_ABONN = CURRENT_DATE() , VALIDITE = 5, DATE_EXPIRATION = '" . $subscriptionNewdateExp . "', IDFORMULE = 10, MONTANT = 5500, NUMEROPAT = '" . $patientInstance->NUMEROPAT . "' WHERE IDPAT = " . $patientInstance->IDPAT;
            $sqlFormuleInsert = "INSERT INTO t_abonnement (IDPAT,
                                                          DATE_ABONN,
                                                          DATE_EXPIRATION,
                                                          DATECREA,
                                                          FLAGSMSABONN,
                                                          NUMRECU,
                                                          IDGERANTP, 
                                                          VALIDITE, 
                                                          MONTANT, 
                                                          IDCENTRE, 
                                                          IDRECU, 
                                                          ORIGINE_ABONN, 
                                                          ALERTDESABONN, 
                                                          FLAGREABONNEMENT, 
                                                          IDFORMULE,
                                                          FLAGMESSAGEVOCAL, 
                                                          NUMEROPAT, 
                                                          NUMEROPAT2, 
                                                          info_sante_id) 
                                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            $stmtFormuleInsert = $databaseInstance->prepare($sqlFormuleInsert);
            $stmtFormuleInsert->bindParam(1, $patientInstance->IDPAT, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(2, $dateInstance->format('Y-m-d'), PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(3, $subscriptionNewdateExp, PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(4, $dateInstance->format('Y-m-d'), PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(5, $flagSmsAbon, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(6, $recuNumber, PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(7, $gerandId, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(8, $validite, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(9, $montant, PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(10, $stmtLastSubscrition->IDCENTRE, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(11, $recuId, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(12, $originAbnt, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(13, $alertDesab, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(14, $flagAbmt, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(15, $formule, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(16, $flagMsgVocal, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(17, $patientInstance->NUMEROPAT, PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(18, $patientInstance->NUMEROPAT2, PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(19, $infoSanteId, PDO::PARAM_STR);
            $stmtFormuleInsert->execute();

            $reponse = [];

            $reponse['code'] = 0;
            $reponse['msg'] = 'Exec result nums: ' . $databaseInstance->lastInsertId();

            return $response->withJson($reponse);
        });

        $this->post('/user/update/pass', function (Request $request, Response $response) {
            $datas = (object)$request->getParsedBody();

            if ($datas->d == 'DEV') $databaseInstance = $this->dev;
            if ($datas->d == 'PROD') $databaseInstance = $this->prod;

            $reponse = [];

            $userId = (int) $datas->userId;
            $userPass = md5($datas->userPass);

            $queryUpdatePass = $databaseInstance->exec("UPDATE patient SET PASSWORD = '$userPass' WHERE IDPAT = $userId ");
            $updateFlag = false;

            $reponse['code'] = 1;
            $reponse['msg'] = 'Echec de la modification !';

            if ($queryUpdatePass == 1) :
                $updateFlag = true;

                $reponse['code'] = 0;
                $reponse['msg'] = 'Modification pris en charge avec succes !';
            endif;

            return $response->withJson($reponse);
        });

        $this->post('/user/send/mail', function (Request $request, Response $response) {
            $datas = (object)$request->getParsedBody();

            $patientInfos = json_decode($datas->patient);
            $object = $datas->object;
            $message = $datas->message;

            if (empty($patientInfos->EMAILPAT)) {
                $patientInfos->EMAILPAT = 'Non defini';
            }


            if (empty($patientInfos->NOMPAT)) {
                $patientInfos->NOMPAT = 'Non defini';
            }

            if (empty($patientInfos->PRENOMPAT)) {
                $patientInfos->PRENOMPAT = 'Non defini';
            }

            if (empty($patientInfos->NUMEROPAT)) {
                $patientInfos->NUMEROPAT = '00000000';
            }

            if (empty($patientInfos->NUMEROPAT)) {
                $patientInfos->NUMEROPAT = '00000000';
            }

            $reponse = [];


            if (envoi_de_la_preoccupation('info@opisms.org', $object, $message, $object, $patientInfos->NOMPAT,
                $patientInfos->PRENOMPAT, $patientInfos->EMAILPAT, $patientInfos->NUMEROPAT)) {

                $reponse['code'] = 0;
                $reponse['msg'] = 'Mail envoye';
            } else {
                $reponse['code'] = 1;
                $reponse['msg'] = 'Mail non envoye';
            }

            return $response->withJson($reponse);
        });

        $this->post('/user/register', function (Request $request, Response $response) {
            $datas = (object) $request->getParsedBody();

            if ($datas->d == 'DEV') {
                $password = md5("2566");
                include '../utils/callapi.php'; // API generateur de login
                $login = rand(111111, 999999);
            }

            if ($datas->d == 'PROD') {
                $password = md5(generatePass());
                include '../utils/callapi.php'; // API generateur de login
                $login = storeLogin(0);
            }

            $centreId = 25;
            $agentId = 1;

            if (!existAbonnement($datas->nPat, $datas->pnPat, $datas->tPat, $datas->d)) :
                if (empty($datas->photoCarnet)) :
                    $datas->photoCarnet = "";
                endif;

                $query = abonPat($login, $datas->ePat, $password, $centreId, $agentId, $datas->nPat, $datas->pnPat,
                    $datas->dtPat, $datas->sxPat, $datas->tPat, $datas->dur, $datas->recu, $datas->fm, $datas->photoCarnet,
                    $datas->preg, $datas->agePreg, $datas->datePregEnd, $datas->abonType, $datas->d);

                if ($query != null):
                    send_email($datas->ePat, 'INSCRIPTION AU CARNET ELECTRONIQUE DE VACCINATION', "NOM: $datas->nPat PRENOMS: $datas->pPat LOGIN: $login PASS: $password");
                    $reponse = array("statut" => 1, "dtExp" => $query['dateExp'], "mt" => $query['montant'], "nuRecu" => $datas->recu, "login" => $login, "idpat" => $query['patID']);
                else :
                    $reponse = array("statut" => 0, "message" => "Erreur Abonnement");
                endif;
            else :
                $reponse = array("statut" => 0, "message" => "Ce client existe deja");
            endif;

            return $response->withJson($reponse);
        });

        $this->post('/user/cinetpay/callback', function (Request $request, Response $response) {
            $cinetpayResponse  = (object) $request->getParsedBody();

            $reponse = [];
            $reponse['code'] = 0;
            $reponse['msg'] = "Od done !";

            /*if (!is_null($cinetpayResponse)) {
                // Check if client payment is done ✅
                $cinetpayTransactionStatus = (object) callApiWithCurl(
                    'https://api-checkout.cinetpay.com/v2/payment/check',
                    '613434',
                    '60554287756c36b71b7e652.62297849',
                    $cinetpayResponse->cpm_trans_id
                );

                if ($cinetpayTransactionStatus->data->status == 'ACCEPTED') {
                    // Fetch row with transaction id for update
                    $clientForUpdate = R::findOne('t_abonnement', ' NUMRECU = "' . $cinetpayResponse->cpm_trans_id . '"');

                    if ($clientForUpdate != null) {
                        $clientForUpdate = (object) $clientForUpdate->export();
                        $clientForUpdateBean = R::load('patient', $clientForUpdate->IDPAT);
                        $clientForUpdateBean->statut = 1;
                        R::store($clientForUpdateBean);
                    }

                    return $response->withJson($reponse);
                }
            }*/
        });

        $this->post('/user/password/reset', function (Request $request, Response $response) {
            // retrieve user mail
            $userEmail = $request->getParsedBodyParam("email", "info@opisms.org");

            if ($request->getParsedBodyParam("d") == 'DEV') $databaseInstance = $this->dev;
            if ($request->getParsedBodyParam("d") == 'PROD') $databaseInstance = $this->prod;

            $reponse = [];

            if ($userEmail == 'info@opisms.org') {
                $reponse['code'] = 0;
                $reponse['message'] = "Impossible de traiter votre demande.";
            } else {
                $newPassword = md5(generatePass());

                if (send_email($userEmail, 'REINITIALISATION MOT DE PASSE', 'Votre nouveau mot de passe: ' . $newPassword)) {
                    $usersWithEmail = $databaseInstance->query("SELECT IDPAT FROM info_patient WHERE EMAILPAT = '" . $userEmail . "'")->fetchAll();

                    if (sizeof($usersWithEmail) > 0) {
                        foreach ($usersWithEmail as $index => $email) {
                            $databaseInstance->exec("UPDATE patient SET PASSWORD = '" . $newPassword . "' WHERE IDPAT = " . $email->IDPAT);
                        }
                        $reponse['code'] = 1;
                        $reponse['msg'] = "Votre demande a bien été prise en compte !";
                    } else {
                        $reponse['code'] = 0;
                        $reponse['msg'] = "Impossible de traiter votre demande.";
                    }
                } else {
                    $reponse['code'] = 0;
                    $reponse['msg'] = "Impossible de traiter votre demande.";
                }
            }


            return $response->withJson($reponse);
        });

    });
// endregion











































































































//region DEVELOPMENT FOR TESTING...
    $app->group('/dev/api/v1', function () {

        $this->get('/tables', function (Request $request, Response $response) {
            $databaseInstance = $this->dev;

            print_r($databaseInstance);
            //return $response->withJson($databaseInstance);
        });


        // User login
        $this->post('/user/login', function (Request $request, Response $response) {
            $reponse = [];
            $datas = (object) $request->getParsedBody();
            $databaseInstance = $this->dev;

            // Get datas user send
            $login = $datas->login;
            $password = $datas->password;

            //  writeToFile($datas);


            $user  = $databaseInstance->query("SELECT * 
                                               FROM info_patient InfPat 
                                                   INNER JOIN patient Pat ON Pat.IDPAT = InfPat.IDPAT 
                                               WHERE (InfPat.EMAILPAT = '" . $login . "' OR Pat.LOGIN = '" . $login . "') AND Pat.PASSWORD = '" . md5($password) . "'")->fetch();


            // Check user status
            if ($user->IDPAT > 0) {
                $reponse['code'] = 0;
                $reponse['msg'] = "User found";

                if ($user->statut == 1) {
                    // Find user infos
                    $sqlUserInfos = $databaseInstance->query("SELECT A.IDPAT, A.DATE_ABONN, A.NUMRECU, A.VALIDITE, A.DATE_EXPIRATION, A.IDFORMULE, A.ID_ABONN
                                                              FROM t_abonnement A
                                                              WHERE A.IDPAT = $user->IDPAT
                                                              ORDER BY A.ID_ABONN DESC LIMIT 1")->fetch();

                    $user->DATE_ABONN = $sqlUserInfos->DATE_ABONN;
                    $user->NUMRECU = $sqlUserInfos->NUMRECU;
                    $user->VALIDITE = $sqlUserInfos->VALIDITE;
                    $user->DATE_EXPIRATION = $sqlUserInfos->DATE_EXPIRATION;
                    $user->IDFORMULE = $sqlUserInfos->IDFORMULE;
                    $reponse['data'] = $user;
                } else {
                    $reponse['code'] = 1;
                    $reponse['msg'] = "Votre compte est inactif";
                }
            } else {
                $reponse['code'] = 2;
                $reponse['msg'] = "Utilisateur inconnu ou Login/mot de passe incorrect !";
            }

            return $response->withJson($reponse);
        });


        $this->post('/user/calendriers', function (Request $request, Response $response) {
            $datas = (object)$request->getParsedBody();
            $patientId = $datas->patientId;

            // Var return
            $reponse = [];

            // Database instance
            $databaseInstance = $this->dev;

            $sqlCalendriers = "SELECT IDCAL, NOMVAC, DATERAPEL, PRESENCE, LOVAC, NOMCENTR, IDPAT 
                FROM calendrier, centre, vaccin 
                WHERE  centre.IDCENTR = calendrier.IDCENTR 
                AND vaccin.IDVAC = calendrier.IDVAC
                AND calendrier.IDPAT = " . $patientId;

            $calendriers = $databaseInstance->query($sqlCalendriers)->fetchAll();

            if (count($calendriers) > 0) {
                $reponse['code'] = 0;
                $reponse['msg'] = "Liste des vaccins effectues";
                $reponse['data'] = [];

                foreach ($calendriers as $index => $calendrier) {
                    array_push($reponse['data'], $calendrier);
                }
            } else {
                $reponse['code'] = 1;
                $reponse['msg'] = 'Pas de vaccin';
            }

            return $response->withJson($reponse);
        });


        $this->post('/user/sms', function (Request $request, Response $response) {
            $datas = (object)$request->getParsedBody();
            $databaseInstance = $this->dev;
            $patientId = $datas->patientId;

            $reponse = [];

            $sqlMessages = "SELECT M.ID_MSG, M.MESSAGE, M.DateMSG, M.HEUREMSG, M.DATE_ENVOI 
                            FROM t_msg_envoyes M, patient P 
                            WHERE (M.IDCLT = " . $patientId . " AND P.IDPAT = M.IDCLT)
                            ORDER BY M.DateMSG DESC";

            $messages = $databaseInstance->query($sqlMessages)->fetchAll();

            if (count($messages) > 0) {
                $reponse['code'] = 0;
                $reponse['msg'] = 'Liste des messages';
                $reponse['data'] = [];

                foreach ($messages as $index => $message) {
                    array_push($reponse['data'], $message);
                }
            } else {
                $reponse['code'] = 1;
                $reponse['msg'] = 'Aucun message enregrtre.';
            }

            return $response->withJson($reponse);
        });


        $this->post('/user/families', function (Request $request, Response $response) {
            $datas = (object) $request->getParsedBody();
            $databaseInstance = $this->dev;
            $patientId = $datas->patientId;

            $reponse = [];

           $sqlFamilies = "SELECT IDPAT, NOMPAT, PRENOMPAT, SEXEPAT, DATEPAT
                           FROM info_patient AS INFPAT
                           WHERE INFPAT.PARENT_ID = " . $patientId . " AND IDPAT != " . $patientId;

            $families = $databaseInstance->query($sqlFamilies)->fetchAll();

            if (count($families) > 0) {
                $reponse['code'] = 0;
                $reponse['msg'] = 'Liste des membres de famille';
                $reponse['data'] = $families;

                foreach ($families as $index => $family) {
                    //  $sqlResponse = $databaseInstance->query("SELECT DATE_ABONN, DATE_EXPIRATION FROM t_abonnement WHERE IDPAT = " .$family->IDPAT. " ORDER BY ID_ABONN DESC LIMIT 1")->fetch();
                    //  $family->DATEABONNEMENT = DateTime::createFromFormat('Y-m-d', $sqlResponse->DATE_ABONN)->format('d-m-Y');
                    //  $family->DATEEXPIRATION = DateTime::createFromFormat('Y-m-d', $sqlResponse->DATE_EXPIRATION)->format('d-m-Y');

                    $reponse['data'][] = $family;
                }
            } else {
                $reponse['code'] = 1;
                $reponse['msg'] = 'Pas de membre';
            }

            return $response->withJson($reponse);
        });


        $this->post('/user/formule/update', function (Request $request, Response $response) {
            $databaseInstance = $this->dev;
            $datas = (object)$request->getParsedBody();
            $patientInstance = json_decode($datas->patient);
            $formatDate = "Y-m-d";

            $flagSmsAbon = 1;
            $originAbnt = 3;
            $flagMsgVocal = 2;
            $infoSanteId = 68;
            $flagAbmt = 2;
            $formule = 10;
            $gerandId = 1;
            $validite = 5;
            $alertDesab = -3;
            $montant = floatval(5500);
            $centreId = 1;
            $recuId = 0;
            $recuNumber = rand(111111, 999999);

            if (is_null($patientInstance->NUMEROPAT2)) {
                $patientInstance->NUMEROPAT2 = "";
            }

            $dateInstance = new DateTime('Africa/Abidjan');
            $dateNow = $dateInstance->format('Y-m-d');
            // Find last subcription
            $sqlLastSubscription = "SELECT * FROM t_abonnement WHERE IDPAT = " . $patientInstance->IDPAT . " ORDER BY ID_ABONN DESC LIMIT 1";
            $stmtLastSubscrition = $databaseInstance->query($sqlLastSubscription)->fetch();

            $subscriptionDateExp = DateTime::createFromFormat($formatDate, $stmtLastSubscrition->DATE_EXPIRATION);
            $dateNow = DateTime::createFromFormat($formatDate, $dateNow);

            if ($dateNow > $subscriptionDateExp) {
                $subscriptionNewdateExp = $dateNow->modify('+ 5years')->format($formatDate);
            } else {
                $subscriptionNewdateExp = $subscriptionDateExp->modify('+ 5years')->format($formatDate);
            }

            //$sqlFormuleUpdate = "UPDATE t_abonnement SET DATE_ABONN = CURRENT_DATE() , VALIDITE = 5, DATE_EXPIRATION = '" . $subscriptionNewdateExp . "', IDFORMULE = 10, MONTANT = 5500, NUMEROPAT = '" . $patientInstance->NUMEROPAT . "' WHERE IDPAT = " . $patientInstance->IDPAT;
            $sqlFormuleInsert = "INSERT INTO t_abonnement (IDPAT,
                                                          DATE_ABONN,
                                                          DATE_EXPIRATION,
                                                          DATECREA,
                                                          FLAGSMSABONN,
                                                          NUMRECU,
                                                          IDGERANTP, 
                                                          VALIDITE, 
                                                          MONTANT, 
                                                          IDCENTRE, 
                                                          IDRECU, 
                                                          ORIGINE_ABONN, 
                                                          ALERTDESABONN, 
                                                          FLAGREABONNEMENT, 
                                                          IDFORMULE,
                                                          FLAGMESSAGEVOCAL, 
                                                          NUMEROPAT, 
                                                          NUMEROPAT2, 
                                                          info_sante_id) 
                                  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            $stmtFormuleInsert = $databaseInstance->prepare($sqlFormuleInsert);
            $stmtFormuleInsert->bindParam(1, $patientInstance->IDPAT, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(2, $dateInstance->format('Y-m-d'), PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(3, $subscriptionNewdateExp, PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(4, $dateInstance->format('Y-m-d'), PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(5, $flagSmsAbon, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(6, $recuNumber, PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(7, $gerandId, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(8, $validite, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(9, $montant, PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(10, $stmtLastSubscrition->IDCENTRE, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(11, $recuId, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(12, $originAbnt, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(13, $alertDesab, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(14, $flagAbmt, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(15, $formule, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(16, $flagMsgVocal, PDO::PARAM_INT);
            $stmtFormuleInsert->bindParam(17, $patientInstance->NUMEROPAT, PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(18, $patientInstance->NUMEROPAT2, PDO::PARAM_STR);
            $stmtFormuleInsert->bindParam(19, $infoSanteId, PDO::PARAM_STR);
            $stmtFormuleInsert->execute();

            $reponse = [];

            $reponse['code'] = 0;
            $reponse['msg'] = 'Exec result nums: ' . $databaseInstance->lastInsertId();

            return $response->withJson($reponse);
        });


        $this->post('/user/update/pass', function (Request $request, Response $response) {
            $datas = (object)$request->getParsedBody();
            $databaseInstance = $this->dev;
            $reponse = [];

            $userId = (int)$datas->userId;
            $userPass = md5($datas->userPass);

            $queryUpdatePass = $databaseInstance->exec("UPDATE patient SET PASSWORD = '$userPass' WHERE IDPAT = $userId ");

            $reponse['code'] = 1;
            $reponse['msg'] = 'Echec de la modification !';

            if ($queryUpdatePass == 1) :
                $updateFlag = true;

                $reponse['code'] = 0;
                $reponse['msg'] = 'Modification pris en charge avec succes !';
            endif;

            return $response->withJson($reponse);
        });


        $this->post('/user/send/mail', function (Request $request, Response $response) {
            $datas = (object) $request->getParsedBody();

            $patientInfos = json_decode($datas->patient);
            $object = $datas->object;
            $message = $datas->message;

            if (empty($patientInfos->EMAILPAT)) {
                $patientInfos->EMAILPAT = 'Non defini';
            }


            if (empty($patientInfos->NOMPAT)) {
                $patientInfos->NOMPAT = 'Non defini';
            }

            if (empty($patientInfos->PRENOMPAT)) {
                $patientInfos->PRENOMPAT = 'Non defini';
            }

            if (empty($patientInfos->NUMEROPAT)) {
                $patientInfos->NUMEROPAT = '00000000';
            }

            if (empty($patientInfos->NUMEROPAT)) {
                $patientInfos->NUMEROPAT = '00000000';
            }

            $reponse = [];


            if (envoi_de_la_preoccupation('info@opisms.org', $object, $message, $object, $patientInfos->NOMPAT,
                $patientInfos->PRENOMPAT, $patientInfos->EMAILPAT, $patientInfos->NUMEROPAT)) {

                $reponse['code'] = 0;
                $reponse['msg'] = 'Mail envoye';
            } else {
                $reponse['code'] = 1;
                $reponse['msg'] = 'Mail non envoye';
            }

            return $response->withJson($reponse);
        });


        $this->post('/user/register', function (Request $request, Response $response) {
            // R::selectDatabase('DEV');

            $datas = (object) $request->getParsedBody();
            $password = md5("2566");

            include '../utils/callapi.php'; // API generateur de login
            $login = rand(111111, 999999);
            //$login = storeLogin(0);

            $centreId = 25;
            $agentId = 1;

            if (!existAbonnement($datas->nPat, $datas->pnPat, $datas->tPat, $datas->datatabase)) :
                if (empty($datas->photoCarnet)) :
                    $datas->photoCarnet = "";
                endif;

                $query = abonPat($login, $datas->ePat, $password, $centreId, $agentId, $datas->nPat, $datas->pnPat,
                                $datas->dtPat, $datas->sxPat, $datas->tPat, $datas->dur, $datas->recu, $datas->fm, $datas->photoCarnet,
                                $datas->preg, $datas->agePreg, $datas->datePregEnd, $datas->abonType, $datas->datatabase);

                if ($query != null):
                    $reponse = array("statut" => 1, "dtExp" => $query['dateExp'], "mt" => $query['montant'], "nuRecu" => $datas->recu, "login" => $login, "idpat" => $query['patID']);
                else :
                    $reponse = array("statut" => 0, "message" => "Erreur Abonnement");
                endif;
            else :
                $reponse = array("statut" => 0, "message" => "Ce client existe deja");
            endif;

            return $response->withJson($reponse);
        });


        $this->post('/user/cinetpay/callback', function (Request $request, Response $response) {
            $cinetpayResponse  = (object) $request->getParsedBody();
            //  R::debug(true, 1);
            /*R::fancyDebug(true);

            //  writeToFile($cinetpayResponse);

            if (!is_null($cinetpayResponse)) {

                // Check if client payment is done ✅
                // https://api-checkout.cinetpay.com/v2/payment/check
                $cinetpayTransactionStatus = (object) callApiWithCurl(
                    'https://api-checkout.cinetpay.com/v2/payment/check',
                    '613434',
                    '60554287756c36b71b7e652.62297849',
                    $cinetpayResponse->cpm_trans_id
                );

                // $responseDecode = json_decode($cinetpayTransactionStatus);

                //  echo $responseDecode->message;

                if ($cinetpayTransactionStatus->data->status == 'ACCEPTED') {
                    // Fetch row with transaction id for update
                    $clientForUpdate = R::findOne('t_abonnement', ' NUMRECU = "' . $cinetpayResponse->cpm_trans_id . '"');

                    if ($clientForUpdate != null) {
                        $clientForUpdate = (object) $clientForUpdate->export();
                        $clientForUpdateBean = R::load('patient', $clientForUpdate->IDPAT);
                        $clientForUpdateBean->statut = 1;
                        R::store($clientForUpdateBean);
                    }
                }
            }*/
        });

        $this->post('/user/password/reset', function (Request $request, Response $response) {
            $databaseInstance = $this->dev;
            // retrieve user mail
            $userEmail = $request->getParsedBodyParam("email", "info@opisms.org");
            $reponse = [];

            if ($userEmail == 'info@opisms.org') {
                $reponse['code'] = 0;
                $reponse['message'] = "Impossible de traiter votre demande.";
            } else {
                $newPassword = md5(generatePass());

                if (send_email($userEmail, 'REINITIALISATION MOT DE PASSE', 'Votre nouveau mot de passe: ' . $newPassword)) {
                    $usersWithEmail = $databaseInstance->query("SELECT IDPAT FROM info_patient WHERE EMAILPAT = '" . $userEmail . "'")->fetchAll();

                    if (sizeof($usersWithEmail) > 0) {
                        foreach ($usersWithEmail as $index => $email) {
                            $databaseInstance->exec("UPDATE patient SET PASSWORD = '" . $newPassword . "' WHERE IDPAT = " . $email->IDPAT);
                        }
                        $reponse['code'] = 1;
                        $reponse['msg'] = "Votre demande a bien été prise en compte !";
                    } else {
                        $reponse['code'] = 0;
                        $reponse['msg'] = "Impossible de traiter votre demande.";
                    }
                } else {
                    $reponse['code'] = 0;
                    $reponse['msg'] = "Impossible de traiter votre demande.";
                }
            }


            return $response->withJson($reponse);
        });
    });
// endregion







    $app->run();
?>