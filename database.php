<?php

    use Slim\App;

    require 'vendor/autoload.php';
    include 'rb.php';

    $settings = [
        'settings' => [
            'displayErrorDetails' => true,
            'prod' => [
                'engine' => 'mysql',
                'host' => 'localhost',
                'database' => 'dbopismsvaccin',
                'username' => 'opismsnet_vaccin',
                'password' => 'JoG4-azXSNWE',
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'options' => [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                    PDO::ATTR_EMULATE_PREPARES => true
                ],
            ],
            'dev' => [
                'engine'    =>  'mysql',
                'host'      =>  'localhost',
                'database'  =>  'opismsne_test__dbopismsvaccin',
                'username'  =>  'opismsnet',
                'password'  =>  '.BDUXVM8Z2*o',
                'charset'   =>  'utf8',
                'collation' =>  'utf8_unicode_ci',
                'options'   =>  [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
                    PDO::ATTR_EMULATE_PREPARES => true
                ]
            ]
        ],
    ];

    $app = new App($settings);

    // Get container
    $container = $app->getContainer();

    // Inject a new instance of PDO in the container
    // Production databse
    $container['prod'] = function ($container) {
        $config = $container->get('settings')['prod'];
        $dsn = "{$config['engine']}:host={$config['host']};dbname={$config['database']};charset={$config['charset']}";
        $username = $config['username'];
        $password = $config['password'];
        return new PDO($dsn, $username, $password, $config['options']);
    };

    // Inject a new instance of PDO in the container
    // Develop database
    $container['dev'] = function ($container) {
        $config = $container->get('settings')['dev'];
        $dsn = "{$config['engine']}:host={$config['host']};dbname={$config['database']};charset={$config['charset']}";
        $username = $config['username'];
        $password = $config['password'];
        return new PDO($dsn, $username, $password, $config['options']);
    };


    //  R::setup('mysql:host=localhost;dbname=opismsne_test__dbopismsvaccin', 'opismsnet', '.BDUXVM8Z2*o');
    R::setup('mysql:host=localhost;dbname=dbopismsvaccin', 'opismsnet_vaccin', 'JoG4-azXSNWE'); // prod
    R::addDatabase("DB2", "mysql:host=localhost;dbname=sigdb", "evoir", "Godis1ONLY1");
    R::addDatabase("DEV", 'mysql:host=localhost;dbname=opismsne_test__dbopismsvaccin', 'opismsnet', '.BDUXVM8Z2*o');
    R::ext('xdispense', function ($type) {
        return R::getRedBean()->dispense($type);
    });

    R::freeze(true);
?>