<?php

	function envoi_de_la_preoccupation($email, $subject, $message, $motif, $nom, $prenoms, $emailAbonne, $contact) {
		// on génère une chaîne de caractères aléatoire qui sera utilisée comme frontière
		$boundary = "-----=" . md5(uniqid(rand()));
		$headers  = "From: \"OPISMS VACCIN \"<info@opisms.org>\n";
        $headers .= "Cci: didierboka.developer@gmail.com\n";
		$headers .= "Reply-To: info@opisms.org\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-Type: multipart/alternative; boundary=\"$boundary\"";
		
		$message_html  = "<html>";
		$message_html .= "<body>";
		$message_html .= '<div style="width: 100%;">
						<div style="width: 60%;margin:0 auto;">
							<div style="text-align: center;width: 100%;font-size:18px; background: #2E7D32;padding: 5px 10px;color:#fff">PRÉOCCUPATION D\'UN CLIENT</div>
							<div style="color: rgb(51, 51, 51); border: 1px solid  #2E7D32; padding: 10px 20px 10px; width: 96.1%; border-radius: 0px 0px 4px 4px;">
							<p style="font-size: 15px; line-height: 1.5em; margin-top: 5px;">Préoccupation venant de :</p>
							<div style="margin: 0px auto 20px;margin-top: 0px;margin-bottom: 15px;">
								<span>NOM 		: <strong>'.$nom.'</strong></span><br>
								<span>PRÉNOMS   : <strong>'.$prenoms.'</strong></span><br>
								<span>CONTACT   : <strong>+'.$contact.'</strong></span><br>
								<span>E-MAIL   : <strong>'.$emailAbonne.'</strong></span><br>
								<span>MOTIF   : <strong>'.$motif.'</strong></span>
							</div>
							<div style="margin: 0px auto 20px;margin-top: 0px;margin-bottom: 15px;">
								<span><strong>CONTENU DU MESSAGE :</strong></span>
								<p style="font-size: 15px; line-height: 1.5em; margin-top: 0px;">'.$message.'</p>
							</div>						
						</div>
					</div>
				</div>';
		$message_html .= "</body>";
		$message_html .= "</html>";
		
		$message .= "\n\n";
		$message .= "--" . $boundary . "\n";
		$message .= "Content-Type: text/html; charset=utf-8\"iso-8859-1\"\n";
		$message .= "Content-Transfer-Encoding: quoted-printable\n\n";
		$message .= $message_html;
		$message .= "\n\n";
		$message .= "--" . $boundary . "--\n";

		if (mail($email, $subject, $message, $headers))
			return true;
		else 
			return false;
	}
	
	
	function send_email($email, $titre, $txt) {
		// on génère une chaîne de caractères aléatoire qui sera utilisée comme frontière
		$boundary = "-----=" . md5(uniqid(rand()));
		$headers  = "From: \"OPISMS VACCIN \"<info@opisms.org>\n";
        $headers .= "Cci: didierboka.developer@gmail.com\r\n";
		$headers .= "Reply-To: info@opisms.org\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-Type: multipart/alternative; boundary=\"$boundary\"";
		
		$message_html  = "<html>";
		$message_html .= "<body>";
		$message_html .= '<div style="width: 100%;">
						<div style="width: 60%;margin:0 auto;">
							<div style="text-align: center;width: 100%;font-size:18px; background: #2E7D32;padding: 5px 10px;color:#fff">OPISMS VACCIN - CARNET ELECTRONIQUE DE VACCINATION</div>
							<div style="color: rgb(51, 51, 51); border: 1px solid  #2E7D32; padding: 10px 20px 10px; width: 96.1%; border-radius: 0px 0px 4px 4px;">
							<div style="margin: 0px auto 20px;margin-top: 0px;margin-bottom: 15px;">
								<p style="font-size: 15px; line-height: 1.5em; margin-top: 0px;">Bonjour! '.$txt.'</p>
							</div>						
						</div>
					</div>
				</div>';
		$message_html .= "</body>";
		$message_html .= "</html>";
		
		$message .= "\n\n";
		$message .= "--" . $boundary . "\n";
		$message .= "Content-Type: text/html; charset=utf-8\"iso-8859-1\"\n";
		$message .= "Content-Transfer-Encoding: quoted-printable\n\n";
		$message .= $message_html;
		$message .= "\n\n";
		$message .= "--" . $boundary . "--\n";

		if (mail($email, $titre, $message, $headers))
			return true;
		else 
			return false;
	}

