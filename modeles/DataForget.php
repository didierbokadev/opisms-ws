<?php

	function verification($UserName, $DateNaiss) {
		
		global $db;
		
		$db->bindMore(array("username" => $UserName, "datepat" => $DateNaiss)); 
				
		$query = $db->row("SELECT COUNT(*) AS total 
		
			FROM patient P, info_patient I WHERE P.IDPAT = I.IDPAT
			
			AND P.LOGIN = :username AND I.DATEPAT = :datepat");
			
		if($query > 0):
		
			return 1;
			
		else :
			return false;
			
		endif;
	}
	
	
	function checkUser($telephone, $password, $naissance) 
	{
 
		global $db;
		
		$db->bindMore(array("telephone" => $telephone, "password" => md5($password), "naissance" => $naissance)); 
				
		$query = $db->row("SELECT P.IDPAT, P.LOGIN, I.NOMPAT, I.PRENOMPAT FROM patient P, info_patient I WHERE P.IDPAT = I.IDPAT
			
			AND I.NUMEROPAT = :telephone 
			
			AND P.PASSWORD = :password 
			
			AND I.DATEPAT = :naissance"
			
		);
			
		if($query != null):
		
			return $query;
			
		else :
		
			return false;
			
		endif;
	}
	
	
	function getIdPatProfil($username){	
		
		global $db;	
		
		$db->bind("username", $username);	
		
		$result = $db->row("SELECT P.IDPAT, P.LOGIN, I.NOMPAT, I.PRENOMPAT 
		
			FROM patient P, info_patient I 
			
			WHERE P.IDPAT = I.IDPAT AND P.LOGIN = :username"
		);
		if($result != null):
		
			return $result;
			
		else :
		
			return false;
			
		endif;	
	}
	
	
	function GeraHash($qtd){ 
		
		$Caracteres = 'ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789'; 
		
		$QuantidadeCaracteres = strlen($Caracteres); 
		
		$QuantidadeCaracteres--; 

		$Hash=NULL; 
		
		for($x=1;$x<=$qtd;$x++){ 
			
			$Posicao = rand(0,$QuantidadeCaracteres); 
			
			$Hash .= substr($Caracteres,$Posicao,1); 
		} 

		return $Hash; 
	}
	
	
	// Mise à jour du password
	function mise_a_jour_code_secret($new_password, $idPat) {
		
		global $db;
		
		$db->query("UPDATE patient SET PASSWORD = :new_password WHERE IDPAT = :idPat"
		
		, array(
				"new_password" => md5($new_password)
				, "idPat" => $idPat
			)
		);
		return true;
	}
	
	function send_reset_email($username, $password, $email) {
		
		/*$subject = "Recuperation Login et mot de passe > OPISMS VACCIN";

		$message="<html><head></head><body><h4>OPISMS VACCIN - Carnet Electronique de vaccination</h4><h2>Bonjour!</h2><p>Vos informations de connexion ci-dessous <p>Identifiant: ".$username."<br />
		* Mot de passe: ".$password."</p><p><a href=\"https://opisms.org/ecarnet\">https://opisms.org</a></p><p>opisms.org - OPISMS VACCIN</p></body></html>";

		// Type de contenu. Ici plusieurs parties de type different "multipart/ALTERNATIVE"
		$headers = "From: \"opisms.org - OPISMS VACCIN\"<info@opisms.org>\n";
		$headers .= "Reply-To: info@opisms.org\n";
		$headers .= "Content-Type: text/html; charset=\"iso-8859-1\"";

		 if (mail($email, $subject, $message, $headers))
			return true;
		else 
			return false;*/
			
			
		// on génère une chaîne de caractères aléatoire qui sera utilisée comme frontière
		$boundary = "-----=" . md5(uniqid(rand()));
		$headers  = "From: \"OPISMS VACCIN \"<info@opisms.org>\n";
		$headers .= "Reply-To: info@opisms.org\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-Type: multipart/alternative; boundary=\"$boundary\"";
		
		$message_html  = "<html>";
		$message_html .= "<body>";
		$message_html .= '<div style="width: 100%;">
						<div style="width: 60%;margin:0 auto;">
							<div style="text-align: center;width: 100%;font-size:18px; background: #2E7D32;padding: 5px 10px;color:#fff">OPISMS VACCIN - CARNET ELECTRONIQUE DE VACCINATION</div>
							<div style="color: rgb(51, 51, 51); border: 1px solid  #2E7D32; padding: 10px 20px 10px; width: 96.1%; border-radius: 0px 0px 4px 4px;">
							<div style="margin: 0px auto 20px;margin-top: 0px;margin-bottom: 15px;">
								<p style="font-size: 15px; line-height: 1.5em; margin-top: 0px;">Bonjour! Vos informations de connexion ci-dessous</p>
								<span>IDENTIFIANT 	 : <strong>'.$username.'</strong></span><br>
								<span>MOT DE PASSE   : <strong>'.$password.'</strong></span><br><br>
								<p style="font-size: 15px; line-height: 1.5em; margin-top: 5px;"><a href=\"https://opisms.org/ecarnet\">https://opisms.org</a></p><p>opisms.org - OPISMS VACCIN</p>								
						</div>
					</div>
				</div>';
		$message_html .= "</body>";
		$message_html .= "</html>";
		
		$message .= "\n\n";
		$message .= "--" . $boundary . "\n";
		$message .= "Content-Type: text/html; charset=utf-8\"iso-8859-1\"\n";
		$message .= "Content-Transfer-Encoding: quoted-printable\n\n";
		$message .= $message_html;
		$message .= "\n\n";
		$message .= "--" . $boundary . "--\n";
		
		$subject = "Recuperation Login et mot de passe > OPISMS VACCIN";

		if (mail($email, $subject, $message, $headers))
			return true;
		else 
			return false;
	}

