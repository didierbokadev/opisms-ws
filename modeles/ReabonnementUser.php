<?php

	//Recupération des informations sur l'abonné
	function user_data($id_pat){	
		global $db;	
		$db->bind("id_pat", $id_pat);	
		$result = $db->row("SELECT A.ID_ABONN, P.IDPAT, I.NOMPAT, I.PRENOMPAT
					, I.DATEPAT, I.NUMEROPAT, I.EMAILPAT, I.ACTIVITE, I.PROVENANCE,
					A.DATE_ABONN, A.NUMRECU, A.VALIDITE, A.DATE_ABONN, I.DATESAISIE,
					P.LOGIN, I.SEXEPAT, A.IDGERANTP, A.IDCENTRE, A.DATECREA
					FROM patient P, info_patient I, t_abonnement A 
					WHERE P.IDPAT = I.IDPAT
					AND P.IDPAT = :id_pat  
					ORDER BY A.ID_ABONN DESC LIMIT 1"
		);
		if($result != null):
			return $result;
		else :
			return false;
		endif;	
	}
	
	
	function verifie_insertion_abonnement($numrecu)
	{	
		global $db;	
		$db->bind("num_recu", $numrecu);	
		$result = $db->row("SELECT * FROM t_abonnement WHERE NUMRECU = :num_recu");
		if($result != null):
			return $result;
		else :
			return false;
		endif;	
	}	
	
	
	function insert_new_t_abonnement($idpat, $date_abonn, $flagsmsabonn, $numrecu, $idgerantp, $validite	
	, $montant, $idcentre, $idrecu, $origine_abonn, $alertdesabonn, $flagreabonnement) {		
		global $db;		
		$db->query("INSERT INTO t_abonnement (IDPAT, DATE_ABONN, DATECREA, FLAGSMSABONN, NUMRECU, IDGERANTP, VALIDITE, MONTANT, IDCENTRE, IDRECU, ORIGINE_ABONN, ALERTDESABONN, FLAGREABONNEMENT) 
		VALUES ('".$idpat."', '".$date_abonn."', NOW(), '".$flagsmsabonn."', '".$numrecu."', '".$idgerantp."', '".$validite."', '".$montant."', '".$idcentre."', '".$idrecu."', '".$origine_abonn."', '".$alertdesabonn."', '".$flagreabonnement."')");
		return true;
	}
	
	
	function actiavtion_du_compte ($statut, $idPat) {		
		global $db;		
		$db->query("UPDATE patient SET statut = :statut WHERE IDPAT = :idPat"		
		, array("statut" => $statut, "idPat" => $idPat)
		);
		return true;
	}
	
	
	function insert_new_t_msg_envoyes($idclt, $message, $datemsg, $heuremsg, $numcell, $etatmsg, $naturemsg, $erreur, $origine, $date_envoi, $differe, $errorsms, $special, $reseau, $idoperateur_saisie, $nomoperateur_saisie, $emailoperateur_saisie) {		
		global $db;		
		$db->query("INSERT INTO t_msg_envoyes (IDCLT, MESSAGE, DateMSG, HEUREMSG, NUMCELL, Etatmsg, NATUREMSG, ERREUR, ORIGINE, DATE_ENVOI, DIFFERE, ERRORSMS, SPECIAL, RESEAU, IDOPERATEUR_SAISIE, NOMOPERATEUR_SAISIE, EMAILOPERATEUR_SAISIE) 
		VALUES ('".$idclt."', '".$message."', '".$datemsg."', '".$heuremsg."', '".$numcell."', '".$etatmsg."', '".$naturemsg."', '".$erreur."', '".$origine."', '".$date_envoi."', '".$differe."', '".$errorsms."', '".$special."', '".$reseau."', '".$idoperateur_saisie."', '".$nomoperateur_saisie."', '".$emailoperateur_saisie."')");
		return true;
	}
	
	
	function reseau($numero) {
		$indicatif = substr($numero, 0, 5);
		switch ($indicatif) :
			case 22507:
			case 22508:
			case 22509:
			case 22547:
			case 22548:
			case 22549:
			case 22557:
			case 22558:
			case 22559:
			case 22577:
			case 22578:
			case 22587:
				$reseau = 'ORANGE';
			break;
			case 22504:
			case 22505:
			case 22506:
			case 22544:
			case 22545:
			case 22546:
			case 22554:
			case 22555:
			case 22556:
			case 22575:
				$reseau = 'MTN';
			break;
			case 22501:
			case 22502:
			case 22503:
			case 22540:
			case 22541:
			case 22542:
				$reseau = 'MOOV';
			break;
			case 22565:
			case 22566:
			case 22567:
				$reseau = 'KOZ';
			break;
			case 22560:
			case 22561:
				$reseau = 'GREEN';
			break;
			case 22569:
				$reseau = 'CAFE';
			break;
			default :
				$reseau = '';
			break;
		endswitch;
		return $reseau;
	}
		
	
	function last_insert_t_msg_envoye($id_pat){	
		global $db;		
		$db->bind("id_pat", $id_pat);	
		$result = $db->row("SELECT * FROM t_msg_envoyes WHERE IDCLT = :id_pat ORDER BY ID_MSG DESC LIMIT 1");
		if($result != null):
			return $result;
		else :
			return false;
		endif;	
	}
	
	
	function mail_de_reabonnement($email, $subject, $txt) 
	{				
		// on génère une chaîne de caractères aléatoire qui sera utilisée comme frontière
		$boundary = "-----=" . md5(uniqid(rand()));
		$headers  = "From: \"OPISMS VACCIN \"<info@opisms.org>\n";
		$headers .= "Reply-To: info@opisms.org\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-Type: multipart/alternative; boundary=\"$boundary\"";
		
		$message_html  = "<html>";
		$message_html .= "<body>";
		$message_html .= '<div style="width: 100%;">
						<div style="width: 60%;margin:0 auto;">
							<div style="text-align: center;width: 100%;font-size:18px; background: #2E7D32;padding: 5px 10px;color:#fff">OPISMS VACCIN - CARNET ELECTRONIQUE DE VACCINATION</div>
							<div style="color: rgb(51, 51, 51); border: 1px solid  #2E7D32; padding: 10px 20px 10px; width: 96.1%; border-radius: 0px 0px 4px 4px;">
							<div style="margin: 0px auto 20px;margin-top: 0px;margin-bottom: 15px;">
								<p style="font-size: 15px; line-height: 1.5em; margin-top: 0px;">'.$txt.'</p>
							</div>						
						</div>
					</div>
				</div>';
		$message_html .= "</body>";
		$message_html .= "</html>";
		
		$message .= "\n\n";
		$message .= "--" . $boundary . "\n";
		$message .= "Content-Type: text/html; charset=utf-8\"iso-8859-1\"\n";
		$message .= "Content-Transfer-Encoding: quoted-printable\n\n";
		$message .= $message_html;
		$message .= "\n\n";
		$message .= "--" . $boundary . "--\n";

		if (mail($email, $subject, $message, $headers))
			return true;
		else 
			return false;
	}



	
