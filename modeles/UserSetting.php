<?php /** @noinspection ALL */

// Login
function login($username, $password) {
    // The instance
    global $db;
    // Bind parameters
    $db->bindMore(array("username" => $username, "password" => $password)); 
    $query = $db->row("SELECT IDPAT FROM patient WHERE LOGIN = :username and PASSWORD = md5(:password)");
    if ($query != null) :
        return $query;
    else :
        return false;
    endif;
}


//Recupération des informations sur l'abonné
function getProfil($id_pat) {
	global $db;	
	$db->bind("id_pat", $id_pat);	
	$result = $db->row("SELECT A.ID_ABONN, P.IDPAT, I.NOMPAT, I.PRENOMPAT
				, I.DATEPAT, I.NUMEROPAT, I.EMAILPAT, I.ACTIVITE, I.PROVENANCE,
				A.DATE_ABONN, A.NUMRECU, A.VALIDITE, A.DATE_ABONN, I.DATESAISIE,
				P.LOGIN, I.SEXEPAT
				FROM patient P, info_patient I, t_abonnement A 
				WHERE P.IDPAT = I.IDPAT
				AND P.IDPAT = :id_pat  
				ORDER BY A.ID_ABONN DESC LIMIT 1"
	);
	if($result != null):
		return $result;
	else :
        return false;
    endif;	
}
