<?php /** @noinspection ALL */

	function send_email($email, $subject, $txt) {		
		/*$subject = $titre;
		$message="<html><head></head><body><h4>OPISMS VACCIN - Carnet Electronique de vaccination</h4><h2>Bonjour!</h2><p>".$txt."</body></html>";
		// Type de contenu. Ici plusieurs parties de type different "multipart/ALTERNATIVE"
		$headers = "From: \"opisms.org - OPISMS VACCIN\"<info@opisms.org>\n";
		$headers .= "Reply-To: info@opisms.org\n";
		$headers .= "Content-Type: text/html; charset=\"iso-8859-1\"";
		 if (mail($email, $subject, $message, $headers))
			return true;
		else 
			return false;*/
			
		// on génère une chaîne de caractères aléatoire qui sera utilisée comme frontière
		$boundary = "-----=" . md5(uniqid(rand()));
		$headers  = "From: \"OPISMS VACCIN \"<info@opisms.org>\n";
		$headers .= "Reply-To: info@opisms.org\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-Type: multipart/alternative; boundary=\"$boundary\"";
		
		$message_html  = "<html>";
		$message_html .= "<body>";
		$message_html .= '<div style="width: 100%;">
						<div style="width: 60%;margin:0 auto;">
							<div style="text-align: center;width: 100%;font-size:18px; background: #2E7D32;padding: 5px 10px;color:#fff">OPISMS VACCIN - CARNET ELECTRONIQUE DE VACCINATION</div>
							<div style="color: rgb(51, 51, 51); border: 1px solid  #2E7D32; padding: 10px 20px 10px; width: 96.1%; border-radius: 0px 0px 4px 4px;">
							<div style="margin: 0px auto 20px;margin-top: 0px;margin-bottom: 15px;">
								<p style="font-size: 15px; line-height: 1.5em; margin-top: 0px;">Bonjour! '.$txt.'</p>
							</div>						
						</div>
					</div>
				</div>';
		$message_html .= "</body>";
		$message_html .= "</html>";
		
		$message .= "\n\n";
		$message .= "--" . $boundary . "\n";
		$message .= "Content-Type: text/html; charset=utf-8\"iso-8859-1\"\n";
		$message .= "Content-Transfer-Encoding: quoted-printable\n\n";
		$message .= $message_html;
		$message .= "\n\n";
		$message .= "--" . $boundary . "--\n";

		if (mail($email, $subject, $message, $headers))
			return true;
		else 
			return false;
	}
	
	function getProfil($idPat){		
		global $db;			
		$db->bind("idPat", $idPat);			
		$result = $db->row("SELECT P.IDPAT, P.LOGIN, I.NOMPAT, I.PRENOMPAT, I.EMAILPAT		
			FROM patient P, info_patient I 			
			WHERE P.IDPAT = I.IDPAT AND P.IDPAT = :idPat"
		);
		if($result != null):		
			return $result;			
		else :		
			return false;			
		endif;	
	}

	// sms recu
	function getSms($idPat) {
		global $db;		
		$db->bind("idPat", $idPat);			
		$query = $db->query("SELECT M.ID_MSG, M.MESSAGE, M.DateMSG, M.HEUREMSG, M.DATE_ENVOI 
			FROM t_msg_envoyes M, patient P 
			WHERE M.IDCLT= :idPat AND P.IDPAT = M.IDCLT");		
		if($query != null) :		
			return $query ;			
		else :		
			return false;			
		endif;
	}
	
	
	//Calendrier vaccinale 
	function getCalendrier($idPat) {
		global $db;		
		$db->bind("idPat", $idPat);			
		$query = $db->query("SELECT IDCAL, NOMVAC, DATERAPEL, PRESENCE, LOVAC, NOMCENTR, IDPAT 
			FROM calendrier, centre, vaccin 
			WHERE  centre.IDCENTR = calendrier.IDCENTR 
			AND vaccin.IDVAC = calendrier.IDVAC
			AND calendrier.IDPAT = :idPat");		
		if($query != null) :		
			return $query ;			
		else :		
			return false;			
		endif;
	} 
	
	
	//Membre de famille 
	function getMembreFamille($idPat) {
		global $db;		
		$db->bindMore(array("idPat" => $idPat, "idPat1" => $idPat)); 		
		$query = $db->query("SELECT IDPAT, NOMPAT, PRENOMPAT, SEXEPAT, DATEPAT FROM info_patient WHERE info_patient.PARENT_ID = :idPat AND IDPAT != :idPat1");		
		if($query != null) :		
			return $query ;			
		else :		
			return false;			
		endif;
	}


