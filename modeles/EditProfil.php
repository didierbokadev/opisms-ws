<?php
	
	function getProfil($idPat){	
		
		global $db;	
		
		$db->bind("idPat", $idPat);	
		
		$result = $db->row("SELECT P.IDPAT, P.LOGIN, I.NOMPAT, I.PRENOMPAT, I.EMAILPAT
		
			FROM patient P, info_patient I 
			
			WHERE P.IDPAT = I.IDPAT AND P.IDPAT = :idPat"
		);
		if($result != null):
		
			return $result;
			
		else :
		
			return false;
			
		endif;	
	}
	
	
	// Mise à jour du profil
	function mise_a_jour_profil($nom_pat, $prenom_pat, $dat_pat, $sex_pat, $activite_pat, $idPat) {
		
		global $db;
		
		$db->query("UPDATE info_patient SET NOMPAT = :nom_pat, 
				PRENOMPAT = :prenom_pat, 
				DATEPAT = :dat_pat ,
				SEXEPAT = :sex_pat ,
				ACTIVITE = :activite_pat 
			WHERE IDPAT = :idPat"
		
		, array(
				"nom_pat" => $nom_pat
				, "prenom_pat" => $prenom_pat
				, "dat_pat" => $dat_pat
				, "sex_pat" => $sex_pat
				, "activite_pat" => $activite_pat
				, "idPat" => $idPat
			)
		);
		return true;
	}

	// Mise à jour du password
	function mise_a_jour_code_secret($new_password, $idPat) {
		
		global $db;
		
		$db->query("UPDATE patient SET PASSWORD = :new_password WHERE IDPAT = :idPat"
		
		, array(
				"new_password" => md5($new_password)
				, "idPat" => $idPat
			)
		);
		return true;
	}
	
	
	function send_email($email, $subject, $txt) {

		/*$message="<html><head></head><body><h4>OPISMS VACCIN - Carnet Electronique de vaccination</h4><h2>Bonjour!</h2><p>".$txt."</body></html>";

		// Type de contenu. Ici plusieurs parties de type different "multipart/ALTERNATIVE"
		$headers = "From: \"opisms.org - OPISMS VACCIN\"<info@opisms.org>\n";
		$headers .= "Reply-To: info@opisms.org\n";
		$headers .= "Content-Type: text/html; charset=\"iso-8859-1\"";

		 if (mail($email, $titre, $message, $headers))
			return true;
		else 
			return false;*/
			
		// on génère une chaîne de caractères aléatoire qui sera utilisée comme frontière
		$boundary = "-----=" . md5(uniqid(rand()));
		$headers  = "From: \"OPISMS VACCIN \"<info@opisms.org>\n";
		$headers .= "Reply-To: info@opisms.org\n";
		$headers .= "MIME-Version: 1.0\n";
		$headers .= "Content-Type: multipart/alternative; boundary=\"$boundary\"";
		
		$message_html  = "<html>";
		$message_html .= "<body>";
		$message_html .= '<div style="width: 100%;">
						<div style="width: 60%;margin:0 auto;">
							<div style="text-align: center;width: 100%;font-size:18px; background: #2E7D32;padding: 5px 10px;color:#fff">OPISMS VACCIN - CARNET ELECTRONIQUE DE VACCINATION</div>
							<div style="color: rgb(51, 51, 51); border: 1px solid  #2E7D32; padding: 10px 20px 10px; width: 96.1%; border-radius: 0px 0px 4px 4px;">
							<div style="margin: 0px auto 20px;margin-top: 0px;margin-bottom: 15px;">
								<p style="font-size: 15px; line-height: 1.5em; margin-top: 0px;">'.$txt.'</p>
							</div>						
						</div>
					</div>
				</div>';
		$message_html .= "</body>";
		$message_html .= "</html>";
		
		$message .= "\n\n";
		$message .= "--" . $boundary . "\n";
		$message .= "Content-Type: text/html; charset=utf-8\"iso-8859-1\"\n";
		$message .= "Content-Transfer-Encoding: quoted-printable\n\n";
		$message .= $message_html;
		$message .= "\n\n";
		$message .= "--" . $boundary . "--\n";

		if (mail($email, $subject, $message, $headers))
			return true;
		else 
			return false;
	}


