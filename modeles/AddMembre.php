<?php

	function verification ($recu, $numero) {		
		global $db;				
		$db->bindMore(array("recu" => $recu, "numero" => $numero));			
		$query = $db->row("SELECT I.IDPAT, I.NOMPAT, I.PRENOMPAT, I.DATEPAT, I.SEXEPAT, COUNT(A.IDPAT) AS total
			FROM info_patient I, t_abonnement A 
			WHERE I.IDPAT = A.IDPAT 
			AND A.NUMRECU = :recu AND I.NUMEROPAT = :numero");			
		if($query != null) :				
			return $query ;						
		else :				
			return false;						
		endif;
	}
	
	function ajout_membre($parent_id, $idPat) {		
		global $db;		
		$db->query("UPDATE info_patient SET PARENT_ID =:parent_id WHERE IDPAT =:idPat"		
		, array(
				"parent_id" => $parent_id
				, "idPat" => $idPat
			)
		);
		return true;
	}
