<?php

require_once 'database.php';
require_once 'imgCarnet.php';

function formatDat($datetoformat) {
    $strings = str_split($datetoformat, 2);
    $dateformated = $strings[0] . '-' . $strings[1] . '-' . $strings[2] . '' . $strings[3];
    return $dateformated;
}


function envoyer_message($from, $numero, $message, $encoding) {
    $from = urlencode($from);
    $messagetex = urlencode($message);
    $numero = urlencode($numero);
    $dataencoding = urlencode($encoding);
    $url = "https://api.infobip.com/api/v3/sendsms/plain?user=opisms01&password=zAgreb2015&Datacoding=" . $dataencoding . "&sender=" . $from . "&SMSText=" . $messagetex . "&GSM=" . $numero . "";
    $ch = file_get_contents($url);
    $tabch = explode("</", $ch);
    $obj = json_decode($ch, true);
    $vSTATUS_M = $tabch[0];
    $vrepid = $tabch[1];
    $STATUS_M = substr($vSTATUS_M, -1);
    $response = substr($vrepid, -18);
    if ($STATUS_M == 0):
        $statut = 1;
    else :
        $statut = 0;
    endif;
    return [$statut, $response];
}


// Get Patient Login
function getPatLogin($numpat) {
    $query = R::getAll("SELECT patient.LOGIN as login
                            FROM patient
                            INNER JOIN info_patient ON (patient.IDPAT = info_patient.IDPAT)
                            WHERE info_patient.NUMEROPAT = :numPat 
                            ORDER BY patient.IDPAT LIMIT 0,5", [":numPat" => $numpat]
    );
    return $query;
}

// Fonction pour Login des USER
function loginUser($login, $password) {

    $query = R::getRow('SELECT `client`.`email`, `client`.`id_client`
      FROM client
      WHERE `client`.`email` = :email AND `client`.`password` = :password',
        [':email' => $login, ':password' => $password]);

    if ($query != null):
        return $query;
    endif;
}


function userInfo($userID) {

    $query = R::getRow('SELECT `utilisateur`.`nom_prenom`
                            FROM utilisateur
                            WHERE `utilisateur`.`client_id` = :clientid AND `utilisateur`.`statut_id`  = :satut',
        [':clientid' => $userID, ':satut' => 1]);

    if ($query != null):
        return $query;
    endif;
}


// Fonctions OPISMS
function checkFJ($idpat) {
    $query = R::getRow("SELECT DATE_FORMAT(calendrier.PRESENCE, '%d-%m-%Y') as datepresence, calendrier.LOVAC as lot, vaccin.NOMVAC as vaccin,calendrier.IMGCARNET as imgcarnet
                                FROM calendrier
                                INNER JOIN vaccin ON vaccin.IDVAC = calendrier.IDVAC
                                WHERE calendrier.IDPAT = :IDPAT 
                                AND calendrier.PRESENCE  <> '0000-00-00' 
                                AND calendrier.IDVAC = 19 
                                ORDER BY calendrier.PRESENCE DESC
                                LIMIT 0,1", [":IDPAT" => $idpat]
    );
    return $query;
}


function searchPat($reqkey, $reqvalue) {
    switch ($reqkey) {
        case 'id':
            $query = R::getAll('SELECT `t_abonnement`.`IDPAT`, `t_abonnement`.`IDFORMULE`, MAX(`t_abonnement`.`DATE_EXPIRATION`) AS DATE_EXPIRATION,`info_patient`.`NOMPAT`,
                                            `info_patient`.`PRENOMPAT`, `info_patient`.`NUMEROPAT`,`info_patient`.`DATEPAT`, `LANGUEPARLEE`,
                                            `info_patient`.`EMAILPAT`,`info_patient`.`SEXEPAT`, `patient`.`LOGIN`,`info_patient`.`PHOTOCARNET` 
                                     FROM t_abonnement
                                     INNER JOIN `info_patient` ON (t_abonnement.IDPAT = info_patient.IDPAT)
                                     INNER JOIN `patient` ON (t_abonnement.IDPAT = patient.IDPAT)
                                     WHERE `info_patient`.`IDPAT` = :idpat
                                     ORDER BY `t_abonnement`.`DATE_EXPIRATION` DESC', [':idpat' => $reqvalue]);
            break;

        case 'recu':
            $query = R::getAll('SELECT `t_abonnement`.`IDPAT`, `t_abonnement`.`IDFORMULE`, `t_abonnement`.`DATE_EXPIRATION`,`info_patient`.`NOMPAT`,
                                            `info_patient`.`PRENOMPAT`, `info_patient`.`NUMEROPAT`,`info_patient`.`DATEPAT`, `LANGUEPARLEE`,
                                            `info_patient`.`EMAILPAT`,`info_patient`.`SEXEPAT`,`patient`.`LOGIN`,`info_patient`.`PHOTOCARNET` 
                                     FROM t_abonnement
                                     INNER JOIN `info_patient` ON (t_abonnement.IDPAT = info_patient.IDPAT)
                                     INNER JOIN `patient` ON (t_abonnement.IDPAT = patient.IDPAT)
                                     WHERE NUMRECU = :numero
                                     ORDER BY `t_abonnement`.`ID_ABONN` DESC', [':numero' => $reqvalue]);
            break;

        case 'tel':
            /* $query = R::getAll( 'SELECT `t_abonnement`.`IDPAT`,`t_abonnement`.`DATE_EXPIRATION`,`info_patient`.`IDPAT`, `info_patient`.`NOMPAT`,`info_patient`.`PRENOMPAT`, `info_patient`.`NUMEROPAT`,`info_patient`.`NUMEROPAT`,`info_patient`.`DATEPAT`,`info_patient`.`EMAILPAT`,`info_patient`.`SEXEPAT`,`patient`.`LOGIN`,`info_patient`.`PHOTOCARNET`
              FROM t_abonnement
              INNER JOIN `info_patient` ON (t_abonnement.IDPAT = info_patient.IDPAT)
              INNER JOIN `patient` ON (info_patient.IDPAT = patient.IDPAT)
              WHERE `info_patient`.`NUMEROPAT`  = :numero OR `info_patient`.`NUMEROPAT2`  = :numero
              GROUP BY `t_abonnement`.`IDPAT`', [':numero' => $reqvalue]); */

            $query = R::getAll('SELECT `t_abonnement`.`IDPAT`, `t_abonnement`.`IDFORMULE`, MAX(`t_abonnement`.`DATE_EXPIRATION`) AS DATE_EXPIRATION,`info_patient`.`IDPAT`, 
                                            `info_patient`.`NOMPAT`,`info_patient`.`PRENOMPAT`, `info_patient`.`NUMEROPAT`,
                                            `info_patient`.`DATEPAT`,`info_patient`.`EMAILPAT`, `LANGUEPARLEE`,
                                            `info_patient`.`SEXEPAT`,`patient`.`LOGIN`/*,`info_patient`.`PHOTOCARNET`*/
                                     FROM t_abonnement
                                     INNER JOIN `info_patient` ON (t_abonnement.IDPAT = info_patient.IDPAT)
                                     INNER JOIN `patient` ON (info_patient.IDPAT = patient.IDPAT)
                                     WHERE `t_abonnement`.`NUMEROPAT`  = :numero OR `t_abonnement`.`NUMEROPAT2`  = :numero
                                     GROUP BY `t_abonnement`.`IDPAT`
                                     ORDER BY `t_abonnement`.`ID_ABONN` DESC', [':numero' => $reqvalue]);
            break;

        case 'login':
            $query = R::getAll('SELECT `t_abonnement`.`IDPAT`, `t_abonnement`.`IDFORMULE`, MAX(`t_abonnement`.`DATE_EXPIRATION`) AS DATE_EXPIRATION,`info_patient`.`IDPAT`, 
                                            `info_patient`.`NOMPAT`,`info_patient`.`PRENOMPAT`, `info_patient`.`NUMEROPAT`,
                                            `info_patient`.`NUMEROPAT`,`info_patient`.`DATEPAT`,`info_patient`.`EMAILPAT`,
                                            `info_patient`.`SEXEPAT`,`patient`.`LOGIN`,`info_patient`.`PHOTOCARNET`, `LANGUEPARLEE`
                                     FROM t_abonnement
                                     INNER JOIN `info_patient` ON (t_abonnement.IDPAT = info_patient.IDPAT)
                                     INNER JOIN `patient` ON (info_patient.IDPAT = patient.IDPAT)
                                     WHERE LOGIN  = :login
                                     ORDER BY `t_abonnement`.`DATE_EXPIRATION` DESC', [':login' => $reqvalue]);
            break;

        case 'passeport':
            $query = R::getAll('SELECT `t_abonnement`.`IDPAT`, `t_abonnement`.`IDFORMULE`, MAX(`t_abonnement`.`DATE_EXPIRATION`) AS DATE_EXPIRATION,`info_patient`.`IDPAT`, 
                                            `info_patient`.`NOMPAT`,`info_patient`.`PRENOMPAT`, `info_patient`.`NUMEROPAT`,
                                            `info_patient`.`NUMEROPAT`,`info_patient`.`DATEPAT`,`info_patient`.`EMAILPAT`,
                                            `info_patient`.`SEXEPAT`,`patient`.`LOGIN`,`info_patient`.`PHOTOCARNET`, `LANGUEPARLEE`
                                    FROM t_abonnement
                                    INNER JOIN `info_patient` ON (t_abonnement.IDPAT = info_patient.IDPAT)
                                    INNER JOIN `patient` ON (info_patient.IDPAT = patient.IDPAT)
                                    WHERE NUMPASSEPORT  = :numpasseport
                                    ORDER BY `t_abonnement`.`DATE_EXPIRATION` DESC', [':numpasseport' => $reqvalue]);
            break;

        default:
            $reqvalues = explode('#', $reqvalue);
            $query = R::getAll('SELECT `t_abonnement`.`IDPAT`, `t_abonnement`.`IDFORMULE`, `t_abonnement`.`DATE_EXPIRATION`,`info_patient`.`NOMPAT`,
                                            `info_patient`.`PRENOMPAT`, `info_patient`.`NUMEROPAT`,`info_patient`.`NUMEROPAT`,
                                            `info_patient`.`DATEPAT`,`info_patient`.`EMAILPAT`,`info_patient`.`SEXEPAT`,
                                            `patient`.`LOGIN`,`info_patient`.`PHOTOCARNET`, `LANGUEPARLEE`
                                     FROM t_abonnement
                                     INNER JOIN `info_patient` ON (t_abonnement.IDPAT = info_patient.IDPAT)
                                     INNER JOIN `patient` ON (info_patient.IDPAT = patient.IDPAT)
                                     WHERE NUMRECU = :recu AND NUMEROPAT = :tel
                                     ORDER BY `t_abonnement`.`DATE_EXPIRATION` DESC', [':recu' => $reqvalues[0], ':tel' => $reqvalues[1]]);
            break;
    }

    return $query;
}


// Fonction pour verifier si l'abonné existe dejà avant de faire un nouvel abonnement
function existAbonnement($nompat, $prenompat, $numero, $database = 'default') {
    R::selectDatabase($database);

    $query = R::getRow('SELECT `info_patient`.`IDPAT`
                             FROM info_patient
                             WHERE NOMPAT = :nompat AND PRENOMPAT = :prenompat AND NUMEROPAT = :numero',
                            [':nompat' => $nompat, ':prenompat' => $prenompat, ':numero' => $numero]);

    if ($query != null):
        return $query;
    endif;
}

function writeToFile($datas) {
    $myfile = fopen("ats_log.txt", "w") or die("Unable to open file!");
    $txt = "dats: " . json_encode($datas) . "\n";
    fwrite($myfile, $txt);
    $txt = "Date : " . gmdate("d-m-Y H:i:s") . "\n";
    fwrite($myfile, $txt);
    $txt = "Contenu : " . strip_tags($_GET['text']) . "\n";
    fwrite($myfile, $txt);
    fclose($myfile);
}

function abonPat($code_patient, $email, $password, $idcentre, $userid, $nompat, $prenompat, $datepat, $sexepat, $numpat,
                 $duree, $numrecu, $formule, $photocarnet, $pregnant, $agePreg, $datePregEnd, $abonnementType, $database = 'default') {

    R::selectDatabase($database);

    $emptyStr = "";
    $emptyDate = "0000-00-00";
    $emptyInt = 0;
    $dateNow = date('Y-m-d');
    $provenance = "APPLICATION_MOBILE";

    if (empty($photocarnet)) {
        $photocarnet = imgCarnet();
    }

    $centreinfo = R::getRow('SELECT centre.IDDIST, district.IDREG
                             FROM centre
                                 INNER JOIN `district` ON (centre.IDDIST = district.IDDIST)
                             WHERE centre.IDCENTR = :idcentre', [':idcentre' => $idcentre]);


    $patientQuery = R::exec('INSERT INTO patient (LOGIN, PASSWORD, DATEALERT, FLAGSMS, statut)
                             VALUES (:login, :password, :datealert, :flagsms, :statut)',
                                [
                                    ":login" => $code_patient,
                                    ":password" => $password,
                                    ":datealert" => 3,
                                    ":flagsms" => 1,
                                    ":statut" => 1
                                ]
                            );

    $patientQueryID = R::getInsertID();

    if ($patientQueryID) {
        if ($pregnant == "Y") {
            $pregnantQuery = R::exec('INSERT INTO gestations (patient_id, 
                                                                   accouchement_assiste_id, 
                                                                   dossier, 
                                                                   naissance, 
                                                                   date_derniere_regle, 
                                                                   date_accouchement, 
                                                                   gestation_debut, 
                                                                   gestation_age, 
                                                                   gestation_terme, 
                                                                   gestation_gestite, 
                                                                   gestation_parite, 
                                                                   naissance_portee, 
                                                                   naissance_vivante, 
                                                                   deces_id)
                                  VALUES (
                                          :patientId, 
                                          :accouchementAssisteId, 
                                          :dossierPreg, 
                                          :naissancePreg, 
                                          :dateDerniereReglePreg,
                                          :dateAccouchementPreg,
                                          :gestationDebutPreg,
                                          :gestationAgePreg,
                                          :gestationTermePreg,
                                          :gestationGestitePreg,
                                          :gestationParitePreg,
                                          :naissancePorteePreg,
                                          :naissanceVivantePreg,
                                          :decesIdPreg
                                          )',

                [
                    ":patientId" => $patientQueryID,
                    ":accouchementAssisteId" => $emptyInt,
                    ":dossierPreg" => $emptyStr,
                    ":naissancePreg" => $emptyInt,
                    ":dateDerniereReglePreg" => $emptyDate,
                    ":dateAccouchementPreg" => $emptyDate,
                    ":gestationDebutPreg" => $dateNow,
                    ":gestationAgePreg" => $agePreg,
                    ":gestationTermePreg" => $datePregEnd,
                    ":gestationGestitePreg" => $emptyInt,
                    ":gestationParitePreg" => $emptyInt,
                    ":naissancePorteePreg" => $emptyInt,
                    ":naissanceVivantePreg" => $emptyInt,
                    ":decesIdPreg" => 5
                ]
            );
        }

        if ($idcentre == 25):
            $infopatientQuery = R::exec('INSERT INTO info_patient (IDPAT,
                                                                IDAGENT,
                                                                IDCENTR,
                                                                IDDIST,
                                                                IDREG,
                                                                NOMPAT,
                                                                PRENOMPAT,
                                                                DATEPAT,
                                                                SEXEPAT,
                                                                NUMEROPAT,
                                                                NUMEROPAT2,
                                                                EMAILPAT,
                                                                NOMMERE,
                                                                AGEMERE,
                                                                LANGUEPARLEE,
                                                                ACTIVITE,
                                                                NIVEAU,
                                                                PROVENANCE,
                                                                NUMDOSSIER,
                                                                PHOTOCARNET,
                                                                DATESAISIE,
                                                                SPECIAL)
                                              VALUES (:idpat,
                                                      :idagent,
                                                      :idcentr,
                                                      :iddist,
                                                      :idreg,
                                                      :nompat,
                                                      :prenompat,
                                                      :datepat,
                                                      :sexepat,
                                                      :numeropat,
                                                      :numeropat2,
                                                      :emailpat,
                                                      :nommere,
                                                      :agemere,
                                                      :langueparlee,
                                                      :activite,
                                                      :niveau,
                                                      :provenance,
                                                      :numdossier,
                                                      :photocarnet,
                                                      :datesaisie,
                                                      :special)',
                                                [":idpat" => $patientQueryID,
                                                    ":idagent" => 1,
                                                    ":idcentr" => 25,
                                                    ":iddist" => $centreinfo['IDDIST'],
                                                    ":idreg" => 26,
                                                    ":nompat" => $nompat,
                                                    ":prenompat" => $prenompat,
                                                    ":datepat" => $datepat,
                                                    ":sexepat" => $sexepat,
                                                    ":numeropat" => $numpat,
                                                    ":numeropat2" => '',
                                                    ":emailpat" => $email,
                                                    ":nommere" => '',
                                                    ":agemere" => '',
                                                    ":langueparlee" => '3',
                                                    ":activite" => '',
                                                    ":niveau" => '',
                                                    ":provenance" => $provenance,
                                                    ":numdossier" => '',
                                                    ":photocarnet" => $photocarnet,
                                                    ":datesaisie" => gmdate('Y-m-d H:i:s'),
                                                    ":special" => 1]
                                        );
        else :
            $infopatientQuery = R::exec('INSERT INTO info_patient (IDPAT,
                                                                IDAGENT,
                                                                IDCENTR,
                                                                IDDIST,
                                                                IDREG,
                                                                NOMPAT,
                                                                PRENOMPAT,
                                                                DATEPAT,
                                                                SEXEPAT,
                                                                NUMEROPAT,
                                                                NUMEROPAT2,
                                                                EMAILPAT,
                                                                NOMMERE,
                                                                AGEMERE,
                                                                LANGUEPARLEE,
                                                                ACTIVITE,
                                                                NIVEAU,
                                                                PROVENANCE,
                                                                NUMDOSSIER,
                                                                PHOTOCARNET,
                                                                DATESAISIE)
                                              VALUES (:idpat,
                                                      :idagent,
                                                      :idcentr,
                                                      :iddist,
                                                      :idreg,
                                                      :nompat,
                                                      :prenompat,
                                                      :datepat,
                                                      :sexepat,
                                                      :numeropat,
                                                      :numeropat2,
                                                      :emailpat,
                                                      :nommere,
                                                      :agemere,
                                                      :langueparlee,
                                                      :activite,
                                                      :niveau,
                                                      :provenance,
                                                      :numdossier,
                                                      :photocarnet,
                                                      :datesaisie)',
                                                [":idpat" => $patientQueryID,
                                                    ":idagent" => 1,
                                                    ":idcentr" => 25,
                                                    ":iddist" => $centreinfo['IDDIST'],
                                                    ":idreg" => 26,
                                                    ":nompat" => $nompat,
                                                    ":prenompat" => $prenompat,
                                                    ":datepat" => $datepat,
                                                    ":sexepat" => $sexepat,
                                                    ":numeropat" => $numpat,
                                                    ":numeropat2" => '',
                                                    ":emailpat" => $email,
                                                    ":nommere" => '',
                                                    ":agemere" => '',
                                                    ":langueparlee" => '3',
                                                    ":activite" => '',
                                                    ":niveau" => '',
                                                    ":provenance" => $provenance,
                                                    ":numdossier" => '',
                                                    ":photocarnet" => $photocarnet,
                                                    ":datesaisie" => gmdate('Y-m-d H:i:s')]
                                            );
        endif;
    }

    if ($infopatientQuery) {
        $dateExp = date('Y-m-d', strtotime('+' . $duree . 'years', strtotime(date('Y-m-d'))));

        $abonnementQuery = R::exec('INSERT INTO t_abonnement (IDGERANTP,
                                                                    IDCENTRE,
                                                                    IDPAT,
                                                                    DATE_ABONN,
                                                                    DATE_EXPIRATION, 
                                                                    NUMRECU,
                                                                    VALIDITE,
                                                                    MONTANT,
                                                                    ORIGINE_ABONN,
                                                                    DATECREA,
                                                                    FLAGSMSABONN,
                                                                    IDRECU,
                                                                    FLAGREABONNEMENT,
                                                                    IDFORMULE,
                                                                    NUMEROPAT,
                                                                    NUMEROPAT2)
                                         VALUES (:idgerantp,
                                                  :idcentre,
                                                  :idpat,
                                                  :date_abonn,
                                                  :date_expiration,
                                                  :numrecu,:validite,
                                                  :montant,
                                                  :origin_abonn,
                                                  :datecrea,
                                                  :flagsmsabonn,
                                                  :idrecu,
                                                  :flagreabonnement,
                                                  :idformule,
                                                  :numeropat,
                                                  :numeropat2)',
            [":idgerantp" => 1,
                ":idcentre" => 25,
                ":idpat" => $patientQueryID,
                ":date_abonn" => date("Y-m-d"),
                ":date_expiration" => $dateExp,
                ":numrecu" => $numrecu,
                ":validite" => $duree,
                ":montant" => $duree * 5500,
                ":origin_abonn" => 2,
                ":datecrea" => date("Y-m-d"),
                ":flagsmsabonn" => 1,
                ":idrecu" => 0,
                ":flagreabonnement" => 1,
                ":idformule" => 10,
                ":numeropat" => $numpat,
                ":numeropat2" => ''
            ]);

        $abonnementQueryID = R::getInsertID();
    }

    if (isset($patientQuery, $infopatientQuery, $abonnementQuery)):
        return array('dateExp' => $dateExp, 'montant' => $duree * 5500, 'patID' => $patientQueryID);
        //return array('dateExp' => $dateExp, 'montant' => 100, 'patID' => $patientQueryID);
    endif;
}

function abonNaissPat($code_patient, $password, $idcentre, $userid, $nompat, $prenompat, $datepat, $sexepat, $numpat, $duree, $numrecu, $formule, $photocarnet, $parentId) {

    $centreinfo = R::getRow('SELECT centre.IDDIST, district.IDREG
                                  FROM centre
                                  INNER JOIN `district` ON (centre.IDDIST = district.IDDIST)
                                  WHERE centre.IDCENTR = :idcentre', [':idcentre' => $idcentre]);

    $patientQuery = R::exec('INSERT INTO patient (LOGIN, PASSWORD, DATEALERT, FLAGSMS, statut)
                                  VALUES (:login, :password, :datealert, :flagsms, :statut)',
                            [
                                ":login" => $code_patient,
                                ":password" => $password,
                                ":datealert" => 3,
                                ":flagsms" => 0,
                                ":statut" => 1
                            ]
    );

    $patientQueryID = R::getInsertID();

    if ($patientQueryID) {

        R::exec('UPDATE gestations 
                      SET date_accouchement = :datePregEnd 
                      WHERE patient_id = :idpat',
            [":datePregEnd" => $datepat, ":idpat" => $parentId]);

        if ($idcentre == 25):
            $infopatientQuery = R::exec('INSERT INTO info_patient (IDPAT,
                                                                        IDAGENT,
                                                                        IDCENTR,
                                                                        IDDIST,
                                                                        IDREG,
                                                                        NOMPAT,
                                                                        PRENOMPAT,
                                                                        DATEPAT,
                                                                        SEXEPAT,
                                                                        NUMEROPAT,
                                                                        NUMEROPAT2,
                                                                        EMAILPAT,
                                                                        NOMMERE,
                                                                        AGEMERE,
                                                                        LANGUEPARLEE,
                                                                        ACTIVITE,
                                                                        NIVEAU,
                                                                        PROVENANCE,
                                                                        NUMDOSSIER,
                                                                        PHOTOCARNET,
                                                                        DATESAISIE,
                                                                        SPECIAL,
                                                                        PARENT_ID)
                                              VALUES (:idpat,
                                                      :idagent,
                                                      :idcentr,
                                                      :iddist,
                                                      :idreg,
                                                      :nompat,
                                                      :prenompat,
                                                      :datepat,
                                                      :sexepat,
                                                      :numeropat,
                                                      :numeropat2,
                                                      :emailpat,
                                                      :nommere,
                                                      :agemere,
                                                      :langueparlee,
                                                      :activite,
                                                      :niveau,
                                                      :provenance,
                                                      :numdossier,
                                                      :photocarnet,
                                                      :datesaisie,
                                                      :special,
                                                      :parentId)',
                [":idpat" => $patientQueryID,
                    ":idagent" => $userid,
                    ":idcentr" => $idcentre,
                    ":iddist" => $centreinfo['IDDIST'],
                    ":idreg" => $centreinfo['IDREG'],
                    ":nompat" => $nompat,
                    ":prenompat" => $prenompat,
                    ":datepat" => $datepat,
                    ":sexepat" => $sexepat,
                    ":numeropat" => $numpat,
                    ":numeropat2" => '',
                    ":emailpat" => '',
                    ":nommere" => '',
                    ":agemere" => '',
                    ":langueparlee" => '3',
                    ":activite" => '',
                    ":niveau" => '',
                    ":provenance" => 'ALIVE TO SMS',
                    ":numdossier" => '',
                    ":photocarnet" => $photocarnet,
                    ":datesaisie" => gmdate('Y-m-d H:i:s'),
                    ":special" => 1,
                    ":parentId" => $parentId]
            );
        else :
            $infopatientQuery = R::exec('INSERT INTO info_patient (IDPAT,
                                                                IDAGENT,
                                                                IDCENTR,
                                                                IDDIST,
                                                                IDREG,
                                                                NOMPAT,
                                                                PRENOMPAT,
                                                                DATEPAT,
                                                                SEXEPAT,
                                                                NUMEROPAT,
                                                                NUMEROPAT2,
                                                                EMAILPAT,
                                                                NOMMERE,
                                                                AGEMERE,
                                                                LANGUEPARLEE,
                                                                ACTIVITE,
                                                                NIVEAU,
                                                                PROVENANCE,
                                                                NUMDOSSIER,
                                                                PHOTOCARNET,
                                                                DATESAISIE,
                                                                PARENT_ID)
                                              VALUES (:idpat,
                                                      :idagent,
                                                      :idcentr,
                                                      :iddist,
                                                      :idreg,
                                                      :nompat,
                                                      :prenompat,
                                                      :datepat,
                                                      :sexepat,
                                                      :numeropat,
                                                      :numeropat2,
                                                      :emailpat,
                                                      :nommere,
                                                      :agemere,
                                                      :langueparlee,
                                                      :activite,
                                                      :niveau,
                                                      :provenance,
                                                      :numdossier,
                                                      :photocarnet,
                                                      :datesaisie, 
                                                      :parentId)',
                [":idpat" => $patientQueryID,
                    ":idagent" => $userid,
                    ":idcentr" => $idcentre,
                    ":iddist" => $centreinfo['IDDIST'],
                    ":idreg" => $centreinfo['IDREG'],
                    ":nompat" => $nompat,
                    ":prenompat" => $prenompat,
                    ":datepat" => $datepat,
                    ":sexepat" => $sexepat,
                    ":numeropat" => $numpat,
                    ":numeropat2" => '',
                    ":emailpat" => '',
                    ":nommere" => '',
                    ":agemere" => '',
                    ":langueparlee" => '3',
                    ":activite" => '',
                    ":niveau" => '',
                    ":provenance" => 'ALIVE TO SMS',
                    ":numdossier" => '',
                    ":photocarnet" => $photocarnet,
                    ":datesaisie" => gmdate('Y-m-d H:i:s'),
                    ":parentId" => $parentId]
            );
        endif;

    }

    if ($infopatientQuery) {
        // formule
        $formuleinfo = R::getRow('SELECT TARIF
                                       FROM formule
                                       WHERE IDFORMULE = :idformule', [':idformule' => $formule]);


        if ($formuleinfo):
            switch ($formule) {
                case '1':
                    $validite = (int)$duree;
                    $montant = ($duree * $formuleinfo['TARIF']);
                    break;

                case '2':
                    $validite = ($duree * 2) + 1;
                    $montant = ($duree * $formuleinfo['TARIF']);
                    break;
                case '3':
                    $validite = ($duree * 2) + 1;
                    $montant = ($duree * $formuleinfo['TARIF']);
                    break;

                default:
                    $validite = (int) $duree;
                    $montant = ($duree * $formuleinfo['TARIF']);
                    break;
            }
        else :
            $validite = (int) $duree;
            $montant = ($duree * 1000);
        endif;

        $dateExp = date('Y-m-d', strtotime('+' . $validite . ' years', strtotime(date('Y-m-d'))));

        $abonnementQuery = R::exec('INSERT INTO t_abonnement (IDGERANTP,
                                                                    IDCENTRE,
                                                                    IDPAT,
                                                                    DATE_ABONN,
                                                                    DATE_EXPIRATION, 
                                                                    NUMRECU,
                                                                    VALIDITE,
                                                                    MONTANT,
                                                                    ORIGINE_ABONN,
                                                                    DATECREA,
                                                                    FLAGSMSABONN,
                                                                    IDRECU,
                                                                    FLAGREABONNEMENT,
                                                                    IDFORMULE,
                                                                    NUMEROPAT,
                                                                    NUMEROPAT2)
                                         VALUES (:idgerantp,
                                                  :idcentre,
                                                  :idpat,
                                                  :date_abonn,
                                                  :date_expiration,
                                                  :numrecu,:validite,
                                                  :montant,
                                                  :origin_abonn,
                                                  :datecrea,
                                                  :flagsmsabonn,
                                                  :idrecu,
                                                  :flagreabonnement,
                                                  :idformule,
                                                  :numeropat,
                                                  :numeropat2)',
            [":idgerantp" => $userid,
                ":idcentre" => $idcentre,
                ":idpat" => $patientQueryID,
                ":date_abonn" => date("Y-m-d"),
                ":date_expiration" => $dateExp,
                ":numrecu" => $numrecu,
                ":validite" => $validite,
                ":montant" => $montant,
                ":origin_abonn" => 0,
                ":datecrea" => date("Y-m-d"),
                ":flagsmsabonn" => 0,
                ":idrecu" => 0,
                ":flagreabonnement" => 1,
                ":idformule" => (int) $formule,
                ":numeropat" => $numpat,
                ":numeropat2" => ''
            ]);

        $abonnementQueryID = R::getInsertID();
    }
    if (isset($patientQuery, $infopatientQuery, $abonnementQuery)):
        return array('dateExp' => $dateExp, 'montant' => $montant, 'patID' => $patientQueryID);
    endif;
}


function reabonPat($idcentre, $idpatient, $duree, $userid, $numrecu, $formule) {

    $oldabonn = R::getRow('SELECT t_abonnement.* ,info_patient.NUMEROPAT
                                FROM t_abonnement
                                INNER JOIN `info_patient` ON (t_abonnement.IDPAT = info_patient.IDPAT)
                                WHERE t_abonnement.IDPAT = :idpatient 
                                ORDER BY ID_ABONN DESC', [':idpatient' => $idpatient]);

    $new_dateabonn = date('Y-m-d');
    //$new_validite = (int)$duree;
    $old_dateabonn = $oldabonn['DATE_ABONN'];
    $old_validite = (int)$oldabonn['VALIDITE'];

    $numPat = $oldabonn['NUMEROPAT'];

    if (isset($oldabonn['DATE_EXPIRATION']) && !empty($oldabonn['DATE_EXPIRATION'])):
        $old_expiration = $oldabonn['DATE_EXPIRATION'];
    else:
        $old_expiration = date('Y-m-d', strtotime('+' . $old_validite . ' years', strtotime($old_dateabonn)));
    endif;

    $formuleinfo = R::getRow('SELECT TARIF
                                   FROM formule
                                   WHERE IDFORMULE = :idformule', [':idformule' => $formule]);
    // die(var_dump($formule));

    if ($formuleinfo):
        switch ($formule) {
            case '1':
                $new_validite = (int)$duree;
                $montant = ($duree * $formuleinfo['TARIF']);
                break;

            case '2':
                $new_validite = ($duree * 2) + 1;
                $montant = ($duree * $formuleinfo['TARIF']);
                break;
            case '3':
                $new_validite = ($duree * 2) + 1;
                $montant = ($duree * $formuleinfo['TARIF']);
                break;

            default:
                $new_validite = (int)$duree;
                $montant = ($duree * $formuleinfo['TARIF']);
                break;
        }
    else:
        $new_validite = (int)$duree;
        $montant = ($duree * 1000);
    endif;

    if ($new_dateabonn > $old_expiration) {
        $dateAbonn = $new_dateabonn;
        $validite = $new_validite;
    } else {
        $dateAbonn = $old_expiration;
        $validite = $new_validite;
    }


    $dateExp = date('Y-m-d', strtotime('+' . $validite . ' years', strtotime($dateAbonn)));

    $queryID = R::exec('INSERT INTO t_abonnement (IDGERANTP, IDCENTRE, IDPAT, DATE_ABONN, DATE_EXPIRATION, NUMRECU,
                                                       VALIDITE, MONTANT, ORIGINE_ABONN, DATECREA, FLAGSMSABONN, IDRECU, 
                                                       FLAGREABONNEMENT, IDFORMULE, NUMEROPAT)
                             VALUES (:idgerantp, :idcentre, :idpat, :date_abonn, :date_expiration, :numrecu, :validite,
                                     :montant, :origin_abonn, :datecrea, :flagsmsabonn, :idrecu, :flagreabonnement, :idformule, 
                                     :numPat)',
        [
            ":idgerantp" => $userid,
            ":idcentre" => $idcentre,
            ":idpat" => $idpatient,
            ":date_abonn" => $dateAbonn,
            ":date_expiration" => $dateExp,
            ":numrecu" => $numrecu,
            ":validite" => $validite,
            ":montant" => $montant,
            ":origin_abonn" => 0,
            ":datecrea" => date("Y-m-d"),
            ":flagsmsabonn" => 0,
            ":idrecu" => 0,
            ":flagreabonnement" => 2,
            ":idformule" => (int)$formule,
            ":numPat" => $numPat
        ]
    );

    $queryID = R::getInsertID();

    if ($queryID):
        R::exec('UPDATE patient SET statut = 1 WHERE IDPAT = :idpat', [":idpat" => $idpatient]);
        R::exec('UPDATE t_abonnement SET FLAGSMSABONN = 1, FLAGREABONNEMENT = 2, ALERTDESABONN = -3 WHERE IDPAT = :idpat', [":idpat" => $idpatient]);
    endif;

    if (strlen($oldabonn['NUMEROPAT']) > 5) :
        $from = 'OPISMS';
        $des = $oldabonn['NUMEROPAT'];
        $expiration = date('d/m/Y', strtotime($dateExp));
        $messagetex = "VOTRE ABONNEMENT AU CARNET ELECTRONIQUE DE VACCINATION EST RECONDUIT JUSQUAU " . $expiration . "";
        $messagetex .= 'INFO 143';
        $from = urlencode($from);
        $messagetex = $messagetex;
    endif;

    R::exec('INSERT INTO t_msg_envoyes(IDCLT, MESSAGE, DateMSG, HEUREMSG, NUMCELL, Etatmsg, NATUREMSG, ERREUR, ORIGINE, 
                                            DATE_ENVOI, DIFFERE, ERRORSMS, SPECIAL, RESEAU, IDOPERATEUR_SAISIE, NOMOPERATEUR_SAISIE, 
                                            EMAILOPERATEUR_SAISIE)
                  VALUES (:idclt, :message, :datemsg, :heuremsg, :numcell, :etatmsg, :naturemsg, :erreur, :origine, :date_envoi, 
                          :differe, :errorsms, :special, :reseau, :idoperateur_saisie, :nomoperateur_saisie, :emailoperateur_saisie)',
        [
            ":idclt" => $idpatient,
            ":message" => $messagetex,
            ":datemsg" => date('Y-m-d'),
            ":heuremsg" => date('H:i:s'),
            ":numcell" => $des,
            ":etatmsg" => 'NT',
            ":naturemsg" => 'RBN',
            ":erreur" => 1,
            ":origine" => 'VAC',
            ":date_envoi" => date('Y-m-d H:i:s'),
            ":differe" => 0,
            ":errorsms" => '',
            ":special" => 0,
            ":reseau" => reseau($des),
            ":idoperateur_saisie" => $userid,
            ":nomoperateur_saisie" => ' ',
            ":emailoperateur_saisie" => ' ',
        ]
    );

    if ($queryID != null):
        return array('dateExp' => $dateExp, 'montant' => $montant, 'numPat' => $oldabonn['NUMEROPAT']);
    endif;

}


function majPat($idpatient, $nompat, $prenompat, $datepat, $numpat, $emailpat, $sexePat, $preg, $agePreg) {
    $queryID = R::exec('UPDATE info_patient 
                        SET 
                                  NOMPAT = :nompat,
                                  PRENOMPAT = :prenompat,
                                  NUMEROPAT = :numpat,
                                  EMAILPAT = :emailpat,
                                  SEXEPAT = :sexePat
                        WHERE IDPAT = :idpat',
        [
            ":idpat" => $idpatient,
            ":nompat" => $nompat,
            ":prenompat" => $prenompat,
            ":numpat" => $numpat,
            ":emailpat" => $emailpat,
            ":sexePat" => $sexePat
        ]
    );

    if ($preg == "Y" || $preg == "FEMME ENCEINTE") {
        $queryPregID = R::exec('UPDATE gestations 
                                 SET
                                        gestation_age = :agePreg
                                 WHERE 
                                        patient_id = :idpat',
                            [
                                ":agePreg" => $agePreg,
                                ":idpat" => $idpatient
                            ]
        );
    }

    if ($queryID != null || $queryPregID != null):
        return true;
    endif;

}


function majVac($userid, $idcentre, $idpatient, $idvaccin, $daterapel, $presence, $lotvac, $imgcarnet, $type) {
    $centreinfo = R::getRow('SELECT centre.IDDIST, district.IDREG
                                  FROM centre
                                  INNER JOIN `district` ON (centre.IDDIST = district.IDDIST)
                                  WHERE centre.IDCENTR = :idcentre', [':idcentre' => $idcentre]);

    if ($type == "scheduler") {
        $daterapel = $daterapel;
    } else {
        $daterapel = $presence;
    }

    $queryID = R::exec('INSERT INTO calendrier(IDAGENT, IDCENTR, IDDIST, IDREG, IDPAT, IDVAC, DATERAPEL, PRESENCE, LOVAC, DATECAL, IMGCARNET, FLAGVALIDATION)
                             VALUES (:idagent, :idcentr, :iddist, :idreg, :idpat, :idvac, :daterapel, :presence, :lovac, :datecal, :imgcarnet, :flagvalidation)',

        [":idagent" => $userid,
            ":idcentr" => $idcentre,
            ":iddist" => $centreinfo['IDDIST'],
            ":idreg" => $centreinfo['IDREG'],
            ":idpat" => $idpatient,
            ":idvac" => $idvaccin,
            ":daterapel" => $daterapel,
            ":presence" => $presence,
            ":lovac" => $lotvac,
            ":imgcarnet" => $imgcarnet,
            ":flagvalidation" => 0,
            ":datecal" => date("Y-m-d")
        ]
    );
    if ($queryID != null):
        return true;
    endif;

}


//  //die($idpatient);
// $now = date('Y-m-d');
//   $query = R::getAll("SELECT GROUP_CONCAT(CONCAT(' ',DATE_FORMAT(calendrier.DATERAPEL, '%d-%m-%y'), '(', vaccin.NOMVAC, ')') ORDER BY calendrier.DATERAPEL) AS resultat FROM calendrier INNER JOIN vaccin ON vaccin.IDVAC = calendrier.IDVAC WHERE calendrier.IDPAT = :IDPAT AND calendrier.DATERAPEL > '2017-10-26' GROUP BY calendrier.IDPAT", [":IDPAT" => $idpatient]);
//   return $query;
// }
function vaccinNextRDV($idpatient) {
    $now = date('Y-m-d');
    $query = R::getAll("SELECT calendrier.IDCAL as idcalendrier, DATE_FORMAT(calendrier.DATERAPEL, '%d-%m-%Y') as daterapel, vaccin.NOMVAC as vaccin, calendrier.FLAGVALIDATION as flagvalidation, calendrier.IMGCARNET as imgcarnet, calendrier.IS_DELETED as is_deleted
                                FROM calendrier
                                INNER JOIN vaccin ON vaccin.IDVAC = calendrier.IDVAC
                                WHERE calendrier.IDPAT = :IDPAT 
                                AND calendrier.DATERAPEL > CURDATE() 
                                AND calendrier.PRESENCE  = '0000-00-00' 
                                AND calendrier.IS_DELETED <> 1
                                GROUP BY calendrier.IDCAL
                                ORDER BY calendrier.DATERAPEL", [":IDPAT" => $idpatient]
    );
    return $query;
}


function vaccinPrevRDV($idpatient) {
    $now = date('Y-m-d');
    $query = R::getAll("SELECT calendrier.IDCAL as idcalendrier, DATE_FORMAT(calendrier.DATERAPEL, '%d-%m-%Y') as daterapel, vaccin.NOMVAC as vaccin, calendrier.FLAGVALIDATION as flagvalidation,calendrier.IMGCARNET as imgcarnet
                                FROM calendrier
                                INNER JOIN vaccin ON vaccin.IDVAC = calendrier.IDVAC
                                WHERE calendrier.IDPAT = :IDPAT 
                                AND calendrier.DATERAPEL < CURDATE() 
                                AND calendrier.PRESENCE = '0000-00-00' 
                                GROUP BY calendrier.IDCAL
                                ORDER BY calendrier.DATERAPEL", [":IDPAT" => $idpatient]
    );
    return $query;
}


function vaccinRDV($idpatient) {
    $now = date('Y-m-d');
    $query = R::getAll("SELECT calendrier.IDCAL as idcalendrier, DATE_FORMAT(calendrier.DATERAPEL, '%d-%m-%Y') as daterapel, DATE_FORMAT(calendrier.PRESENCE, '%d-%m-%Y') as datepresence, centre.NOMCENTR as centre, vaccin.NOMVAC as vaccin, calendrier.LOVAC as lot, calendrier.FLAGVALIDATION as flagvalidation,calendrier.IMGCARNET as imgcarnet
                                FROM calendrier
                                INNER JOIN vaccin ON vaccin.IDVAC = calendrier.IDVAC
                                INNER JOIN centre ON calendrier.IDCENTR = centre.IDCENTR
                                WHERE calendrier.IDPAT = :IDPAT 
                                AND calendrier.DATERAPEL != '0000-00-00' 
                                AND calendrier.PRESENCE != '0000-00-00' 
                                GROUP BY calendrier.IDCAL
                                ORDER BY calendrier.DATERAPEL", [":IDPAT" => $idpatient]
    );
    return $query;
}


function majRdvVac($calid, $userid, $idcentre, $idpatient, $presence, $lotvac, $imgcarnet) {

    $centreinfo = R::getRow('SELECT centre.IDDIST, district.IDREG
      FROM centre
      INNER JOIN `district` ON (centre.IDDIST = district.IDDIST)
      WHERE centre.IDCENTR = :idcentre', [':idcentre' => $idcentre]);
    //die(var_dump($centreinfo));
    $queryID = R::exec('UPDATE calendrier 
                  SET 
                  IDAGENT = :idagent,
                  IDCENTR = :idcentr,
                  IDDIST = :iddist,
                  IDREG = :idreg,
                  IDPAT = :idpat,
                  PRESENCE = :presence,
                  LOVAC = :lovac,
                  DATECAL = :datecal,
                  IMGCARNET = :imgcarnet,
                  FLAGVALIDATION = :flagvalidation
                  WHERE IDCAL = :idcal',
        [
            ":idcal" => $calid,
            ":idagent" => $userid,
            ":idcentr" => $idcentre,
            ":iddist" => $centreinfo['IDDIST'],
            ":idreg" => $centreinfo['IDREG'],
            ":idpat" => $idpatient,
            ":presence" => $presence,
            ":lovac" => $lotvac,
            ":imgcarnet" => $imgcarnet,
            ":flagvalidation" => 0,
            ":datecal" => date("Y-m-d")
        ]);

    // $queryID =  R::exec('UPDATE info_patient
    //                 SET
    //                 NOMPAT = :nompat,
    //                 PRENOMPAT = :prenompat,
    //                 DATEPAT = :datepat,
    //                 NUMEROPAT = :numpat,
    //                 EMAILPAT = :emailpat
    //                 WHERE IDPAT = :idpat',
    //                 [
    //                   ":idpat" => $idpatient,
    //                   ":nompat" => $nompat,
    //                   ":prenompat" => $prenompat,
    //                   ":datepat" => $datepat,
    //                   ":numpat" => $numpat,
    //                   ":emailpat" => $emailpat
    //                 ]);

    if ($queryID != null):
        return true;
    endif;

}


function validVac($calid, $userid, $idcentre, $idpatient) {

    $centreinfo = R::getRow('SELECT centre.IDDIST, district.IDREG
      FROM centre
      INNER JOIN `district` ON (centre.IDDIST = district.IDDIST)
      WHERE centre.IDCENTR = :idcentre', [':idcentre' => $idcentre]);
    //die(var_dump($centreinfo));
    $queryID = R::exec('UPDATE calendrier 
                  SET 
                  IDAGENT = :idagent,
                  IDCENTR = :idcentr,
                  IDDIST = :iddist,
                  IDREG = :idreg,
                  IDPAT = :idpat,
                  FLAGVALIDATION = :flagvalidation
                  WHERE IDCAL = :idcal',
        [
            ":idcal" => $calid,
            ":idagent" => $userid,
            ":idcentr" => $idcentre,
            ":iddist" => $centreinfo['IDDIST'],
            ":idreg" => $centreinfo['IDREG'],
            ":idpat" => $idpatient,
            ":flagvalidation" => 1,
        ]);

    if ($queryID != null):
        return true;
    endif;

}


function reseau($numero) {
    $indicatif = substr($numero, 0, 5);
    switch ($indicatif) :
        case 22507:
        case 22508:
        case 22509:
        case 22547:
        case 22548:
        case 22549:
        case 22557:
        case 22558:
        case 22559:
        case 22567:
        case 22568:
        case 22569:
        case 22577:
        case 22578:
        case 22579:
        case 22587:
        case 22588:
        case 22589:
        case 22597:
            $reseau = 'ORANGE';
            break;
        case 22504:
        case 22505:
        case 22506:
        case 22544:
        case 22545:
        case 22546:
        case 22554:
        case 22555:
        case 22556:
        case 22564:
        case 22565:
        case 22566:
        case 22574:
        case 22575:
        case 22576:
            $reseau = 'MTN';
            break;
        case 22501:
        case 22502:
        case 22503:
        case 22540:
        case 22541:
        case 22542:
            $reseau = 'MOOV';
            break;
        default :
            $reseau = '';
            break;
    endswitch;

    return $reseau;
}


function numrecu() {
    // counter

    // Open the file for reading
    $fp = fopen("counter.txt", "r");
    flock($fp, LOCK_EX);

    // Get the existing count
    $count = fread($fp, 1024);

    // Close the file
    fclose($fp);

    // Add 1 to the existing count
    $count = $count + 1;

    // Display the number of hits
    // If you don't want to display it, comment out this line

    // Reopen the file and erase the contents
    $fp = fopen("counter.txt", "w");

    // Write the new count to the file
    fwrite($fp, $count);
    fflush($fp);
    // flush output before releasing the lock
    flock($fp, LOCK_UN);

    // Close the file
    fclose($fp);

    $numrecu = date('y') . 'Z' . str_pad($count, 4, "0", STR_PAD_LEFT);
    return $numrecu;
}


function generateRecu() {
    $myrecu = R::getRow('SELECT myrecu.value, myrecu.id
                              FROM myrecu
                              ORDER BY  myrecu.id DESC'
                        );

    //die(var_dump($myrecu));
    $queryID = R::exec('UPDATE myrecu 
                             SET value = :value, updated_at = :dateupdate
                             WHERE id = :idmyrecu',
        [
            ":idmyrecu" => $myrecu['id'], ":value" => $myrecu['value'] + 1, ":dateupdate" => gmdate("Y-m-d H:i:s")
        ]);

    if ($queryID != null):
        $numrecu = date('y') . 'Z' . str_pad($myrecu['value'] + 1, 4, "0", STR_PAD_LEFT);
        return $numrecu;
    endif;
}


function generatePass($nbChar = 4) {
    if (empty($nbChar)) {
        $nbChar = 4;
    }

    $characters = '0123456789';
    $firstPart = substr(str_shuffle($characters), 0, $nbChar);
    return str_shuffle($firstPart);
}


function callApiWithCurl($url, $siteId, $apiKey, $transactionId) {
    $curl = curl_init();

    $params = "apikey=$apiKey&site_id=$siteId&transaction_id=$transactionId";

    curl_setopt_array(
        $curl,
        array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded'
            ),
        )
    );

    $apiResponse = curl_exec($curl);
    $apiErr = curl_error($curl);
    curl_close($curl);

    if ($apiErr == null) return json_decode($apiResponse); else return $apiErr;
}
